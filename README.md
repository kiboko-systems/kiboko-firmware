Kiboko Firmware
===============



# Changelog

## V17.0.1
* anpassung der Phototdetektor Algorithmus
  Es wird nun mit einem fixen Zeitraster gemessen, das definiert wird durch den
  ersten Puls, alle weiteren Pulse müssen innerhalb von n*dT +-2*dt liegen,
  wobei hier dT die fixe Periode von 900,1000 oder 1100 us ist, und dt die
  eingestellte Jitter Toleranz.
* Einstellungen der Bootbox werden nun an den PC übertragen.
  Je eine Einstellung pro Alive-Paket, zyklisch und ständig.
* Einstellungen in der Bootbox können nun in den Flash gespeichert und von
  diesem wieder geladen.
* Erweiterung der packet.h

## V17.0.0
* changed packet.h to set filter parameters
* Boat Box: add command "printpf" for printing photo filter settings
* Trigger Station: linked LED-pulse to local system time pitch (100us)
* Software PLL --> Jitter and Offet: max. +-500ns, measured on the 100ms sync-pulse
* finetuning of transmission delays
* clock drift less than 0.2ppm between the devices without sync connection


## V4.4
git Started from Revision 4.4
