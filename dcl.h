/*
 * Module for
 * Debug Command Line
 *
 * dcl.h
 *
 *  Created on: 28.06.2012
 *      Author: karl
 *
 * usage:
 * 		initalize TRX-objects in your main programm
 * 		call afterwards the DCL_Init() function.
 * 		If you want to support the GD0-Pin, forward the ISR
 * 		to the DCL_Gd0ChangedHandler() with the correct obejct.
 * 		if possible, avoid direct forwarding from the ISR, and use
 * 		software-flags in the main-loop instead.
 *
 * 		call the DCL_FeedWithChar() when ever a character is received on the dbgu!
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_

#include "defs.h"

#include <stdbool.h>
#include <trx/trx.h>
#include <stdbool.h>

typedef enum {DCL_cNone, DCL_cStart, DCL_cStop, DCL_cClear, DCL_cHelp, DCL_cPrintrf, DCL_cVolt, DCL_cReboot,
	DCL_cSt, DCL_cSetreg, DCL_cGetreg, DCL_cGetsreg, DCL_cSetpa, DCL_cGetpa, DCL_cRssi,
	DCL_cCmd, DCL_cRfreset,
	DCL_cFifowrite, DCL_cFiforead, DCL_cGetfifo, DCL_cFifowritetest, DCL_cFlushrx, DCL_cFlushtx,
	DCL_cNum2rssi, DCL_cCheckregs, DCL_cName, DCL_cSettime, DCL_cGettime, DCL_cDebug,
	DCL_cDummytx, DCL_cPrintrssi, DCL_cRecord, DCL_cBreak, DCL_cGetbuffer,
	DCL_cMatrixTestStart, DCL_cMatrixTestStop, DCL_cMatrixRainStart, DCL_cMatrixRainStop,
	DCL_cPrintpf
}DCL_Command;

void DCL_Init(TRX_Object* trxA, TRX_Object* trxB);
DCL_Command DCL_FeedWithChar(char c);

// call these handlers from the ISR in the main programm
void DCL_Gd0ChangedHandler(TRX_Object* pRf);
void DCL_ButtonChangedHandler(bool isPressed);
int16_t DCL_num2rssi(uint8_t num);


#endif /* CONSOLE_H_ */

