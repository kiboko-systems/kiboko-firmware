/*
 * rfswshared.h
 *
 *  Created on: 06.07.2012
 *      Author: Karl Zeilhofer
 */

#ifndef RFSWSHARED_H_
#define RFSWSHARED_H_

#include "defs.h"

extern volatile TIME_T time_100us;
extern volatile bool pitFlag; // this flag is set by the PIT-interrupt, it is cleared in any of the main-loops
extern float v1,v2,v3,vMin;
extern AT91PS_DBGU pDbgu;
extern AT91PS_USART pUsartRS232;

void InitVoltageFilters();
void UpdateVoltages();
char* sprintHMSX(TIME_T t);
void PrintVoltages();
int VoltageToPercent(float v);
void PrintTime(TIME_T t); // print a human readable time, without a new-line

#endif /* RFSWSHARED_H_ */
