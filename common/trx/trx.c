// Source File for the Texas Instruments Transceiver Chips
// especially for the CC2500 and the CC1101
// Karl Zeilhofer
// 8.5.2012

// this driver currently only supports 16-bit writes/reads to/from the SPI

#include "trx.h"
#include <string.h>
#include <spi/spi.h>
#include <pio/pio_it.h>
#include <utility/trace.h>

#define KICK_DOG 	(AT91C_BASE_WDTC->WDTC_WDCR = 0xA5000001)

#define BURST_BIT ((uint8_t)(1<<6))
#define READ_WRITEn_BIT ((uint8_t)(1<<7))
#define ADDRESS_MASK ((uint8_t)0x3F) // use this for masking a register address
#define WAIT_FOR_STATE_TIMEOUT 5000 // when waiting for state for too long time, halt the software

extern volatile uint32_t time_100us;
// prototypes of private functions:
static uint16_t spiWrite(TRX_Object* self, uint16_t txData);
static void resetChip(TRX_Object* self);
static uint8_t readStatusRegister(TRX_Object* self, TRX_STATUS_REGISTER regAddress);
void wait(uint32_t microSeconds);



// has to be run before using the trx object in other functions.
// configure the pinset and prepare the registers
TRX_Object* TRX_constructor(TRX_Object* self, TRX_Pinset pinSet, TRX_Config config)
{
	self->settings.spiBase = BOARD_SPI_BASE;
	self->settings.spiID = BOARD_SPI_ID;


	switch(pinSet)
	{
		case TRX_PINSET_A:
		{
			strcpy(self->name, "TRX-Module A");
			self->settings.npcs = BOARD_TRX_A_NPCS;
			self->settings.spiBitrate = BOARD_TRX_A_SPI_CLK;
			TRX_Pins pins = {BOARD_TRX_A_PINS}; // use pins for TRX-Module A
			self->settings.pins = pins;
		}break;
		case TRX_PINSET_B:
		{
			strcpy(self->name, "TRX-Module B");
			self->settings.npcs = BOARD_TRX_B_NPCS;
			self->settings.spiBitrate = BOARD_TRX_B_SPI_CLK;
			TRX_Pins pins = {BOARD_TRX_B_PINS}; // use pins for TRX-Module B
			self->settings.pins = pins;
		}break;
	}
	// clear arrays:
	for(int n=0; n<TRX_NUM_CR; n++)
	{
		self->settings.regConfigured[n]=0;
		self->settings.configRegs[n]=0;
		self->settings.readonly[n]=0; // set to writeable by default
	}

	// set some registers to read only, because in the datasheet it
	// is recommendet not to write into self registers
	self->settings.readonly[TRX_CR_FSTEST]   = 1;
	self->settings.readonly[TRX_CR_PTEST]    = 1;
	self->settings.readonly[TRX_CR_AGCTEST]  = 1;


//	// lansers config0:
//	TRX_setSettingsReg(self, TRX_CR_IOCFG2, 0x29);//0x29;
////	TRX_setSettingsReg(self, TRX_CR_IOCFG1, 0x06);//0x2e;
//	TRX_setSettingsReg(self, TRX_CR_IOCFG0, 0x06);//0x2e;//0x06;
////	TRX_setSettingsReg(self, TRX_CR_FIFOTHR, //0x07;
////	TRX_setSettingsReg(self, TRX_CR_SYNC1, //0xd3;
////	TRX_setSettingsReg(self, TRX_CR_SYNC0, //0x91;
//	TRX_setSettingsReg(self, TRX_CR_PKTLEN, 0x3D); // 62 = 64 - 2 status bytes
//	TRX_setSettingsReg(self, TRX_CR_PKTCTRL1, 0x04); // no address check, append status
//	TRX_setSettingsReg(self, TRX_CR_PKTCTRL0, 0x05); // variable length, crc enabled
//	TRX_setSettingsReg(self, TRX_CR_ADDR, 0x00);//0x00;
//	TRX_setSettingsReg(self, TRX_CR_CHANNR, 0x00);//0x00;
//	TRX_setSettingsReg(self, TRX_CR_FSCTRL1, 0x08);//0x0a;
//	TRX_setSettingsReg(self, TRX_CR_FSCTRL0, 0x00);//0x00;
//	TRX_setSettingsReg(self, TRX_CR_FREQ2, 0x5D);//0x5d;
//	TRX_setSettingsReg(self, TRX_CR_FREQ1, 0x93);//0x93;
//	TRX_setSettingsReg(self, TRX_CR_FREQ0, 0xB1);//0xb1;
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG4, 0x86);//0x2d;
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG3, 0x83);//0x3b;
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG2, 0x03);//0x73;
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG1, 0x22);//0x22;
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG0, 0xF8);//0xf8;
//	TRX_setSettingsReg(self, TRX_CR_DEVIATN, 0x44);//0x00;
////	TRX_setSettingsReg(self, TRX_CR_MCSM2]    = //0x07;
////	TRX_setSettingsReg(self, TRX_CR_MCSM1]    = //0x30;
//	TRX_setSettingsReg(self, TRX_CR_MCSM0, 0x08);//0x18;   // std= 0x18; 0x08 = MCSM0 (never calibrate)
//	TRX_setSettingsReg(self, TRX_CR_FOCCFG, 0x16);//0x1d;
//	TRX_setSettingsReg(self, TRX_CR_BSCFG, 0x6C);//0x1c;
//	TRX_setSettingsReg(self, TRX_CR_AGCCTRL2, 0x03);//0xc7;
//	TRX_setSettingsReg(self, TRX_CR_AGCCTRL1, 0x40);//0x00;
//	TRX_setSettingsReg(self, TRX_CR_AGCCTRL0, 0x91);//0xb0;
////	TRX_setSettingsReg(self, TRX_CR_WOREVT1]  = //0x87;
////	TRX_setSettingsReg(self, TRX_CR_WOREVT0]  = //0x6b;
////	TRX_setSettingsReg(self, TRX_CR_WORCTRL]  = //0xf8;
//	TRX_setSettingsReg(self, TRX_CR_FREND1, 0x56);//0xb6;
//	TRX_setSettingsReg(self, TRX_CR_FREND0, 0x10);//0x10;
//	TRX_setSettingsReg(self, TRX_CR_FSCAL3, 0xA9);//0xea;
//	TRX_setSettingsReg(self, TRX_CR_FSCAL2, 0x0A);//0x0a;
//	TRX_setSettingsReg(self, TRX_CR_FSCAL1, 0x00);//0x00;
//	TRX_setSettingsReg(self, TRX_CR_FSCAL0, 0x11);//0x11;
////	TRX_setSettingsReg(self, TRX_CR_RCCTRL1]  = //0x41;
////	TRX_setSettingsReg(self, TRX_CR_RCCTRL0]  = //0x00;
//	TRX_setSettingsReg(self, TRX_CR_FSTEST, 0x59);//0x59; // read only
////	TRX_setSettingsReg(self, TRX_CR_PTEST]    = //0x7f;
////	TRX_setSettingsReg(self, TRX_CR_AGCTEST]  = //0x3f;
//	TRX_setSettingsReg(self, TRX_CR_TEST2, 0x88);//0x88;
//	TRX_setSettingsReg(self, TRX_CR_TEST1, 0x31);//0x31;
//	TRX_setSettingsReg(self, TRX_CR_TEST0, 0x0B);//0x0b;



//// 500 kBit/s (RfStudio):
//	// Sync word qualifier mode = 30/32 sync word bits detected
//	// CRC autoflush = false
//	// Channel spacing = 199.951172
//	// Data format = Normal mode
//	// Data rate = 499.878
//	// RX filter BW = 812.500000
//	// Preamble count = 8
//	// Address config = No address check
//	// Whitening = false
//	// Carrier frequency = 2432.999908
//	// Device address = 0
//	// TX power = 0
//	// Manchester enable = true
//	// CRC enable = true
//	// Phase transition time = 0
//	// Modulation format = MSK
//	// Base frequency = 2432.999908
//	// Modulated = true
//	// Channel number = 0
//
//	// Control Registers
//
//	TRX_setSettingsReg(self, TRX_CR_IOCFG2,   0x29);
//	TRX_setSettingsReg(self, TRX_CR_IOCFG1,   0x2e);
//	TRX_setSettingsReg(self, TRX_CR_IOCFG0,   0x06); // assert on finished packet tx/rx
//	TRX_setSettingsReg(self, TRX_CR_FIFOTHR,  0x07);
//	TRX_setSettingsReg(self, TRX_CR_SYNC1,    0xd3);
//	TRX_setSettingsReg(self, TRX_CR_SYNC0,    0x91);
//	TRX_setSettingsReg(self, TRX_CR_PKTLEN,   0x3D); // 62 Bytes
//	TRX_setSettingsReg(self, TRX_CR_PKTCTRL1, 0x04); // no address check, append status
//	TRX_setSettingsReg(self, TRX_CR_PKTCTRL0, 0x05); // variable length, crc enabled
//	TRX_setSettingsReg(self, TRX_CR_ADDR,     0x00);
//	TRX_setSettingsReg(self, TRX_CR_CHANNR,   0x00);
//	TRX_setSettingsReg(self, TRX_CR_FSCTRL1,  0x0c);
//	TRX_setSettingsReg(self, TRX_CR_FSCTRL0,  0x00);
//	TRX_setSettingsReg(self, TRX_CR_FREQ2,    0x5d);
//	TRX_setSettingsReg(self, TRX_CR_FREQ1,    0x93);
//	TRX_setSettingsReg(self, TRX_CR_FREQ0,    0xb1);
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG4,  0x0e);
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG3,  0x3b);
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG2,  0x7b);
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG1,  0x42);
//	TRX_setSettingsReg(self, TRX_CR_MDMCFG0,  0xf8);
//	TRX_setSettingsReg(self, TRX_CR_DEVIATN,  0x00);
//	TRX_setSettingsReg(self, TRX_CR_MCSM2,    0x07);
//	TRX_setSettingsReg(self, TRX_CR_MCSM1,    0x30);
//	TRX_setSettingsReg(self, TRX_CR_MCSM0,    0x18);
//	TRX_setSettingsReg(self, TRX_CR_FOCCFG,   0x1d);
//	TRX_setSettingsReg(self, TRX_CR_BSCFG,    0x1c);
//	TRX_setSettingsReg(self, TRX_CR_AGCCTRL2, 0xc7);
//	TRX_setSettingsReg(self, TRX_CR_AGCCTRL1, 0x40);
//	TRX_setSettingsReg(self, TRX_CR_AGCCTRL0, 0xb0);
//	TRX_setSettingsReg(self, TRX_CR_WOREVT1,  0x87);
//	TRX_setSettingsReg(self, TRX_CR_WOREVT0,  0x6b);
//	TRX_setSettingsReg(self, TRX_CR_WORCTRL,  0xf8);
//	TRX_setSettingsReg(self, TRX_CR_FREND1,   0xb6);
//	TRX_setSettingsReg(self, TRX_CR_FREND0,   0x10);
//	TRX_setSettingsReg(self, TRX_CR_FSCAL3,   0xea);
//	TRX_setSettingsReg(self, TRX_CR_FSCAL2,   0x0a);
//	TRX_setSettingsReg(self, TRX_CR_FSCAL1,   0x00);
//	TRX_setSettingsReg(self, TRX_CR_FSCAL0,   0x19);
//	TRX_setSettingsReg(self, TRX_CR_RCCTRL1,  0x41);
//	TRX_setSettingsReg(self, TRX_CR_RCCTRL0,  0x00);
//	TRX_setSettingsReg(self, TRX_CR_FSTEST,   0x59);
//	TRX_setSettingsReg(self, TRX_CR_PTEST,    0x7f);
//	TRX_setSettingsReg(self, TRX_CR_AGCTEST,  0x3f);
//	TRX_setSettingsReg(self, TRX_CR_TEST2,    0x88);
//	TRX_setSettingsReg(self, TRX_CR_TEST1,    0x31);
//	TRX_setSettingsReg(self, TRX_CR_TEST0,    0x0b);


	switch(config)
	{
		case TRX_CONFIG_CC2500:
		{
			strcat(self->name, " (CC2500)");
			self->settings.patable = 0xff; // =  1dBm
	// 250kbit/s (Rf-Studio):
		// Sync word qualifier mode = 30/32 sync word bits detected
		// CRC autoflush = false
		// Channel spacing = 199.951172
		// Data format = Normal mode
		// Data rate = 249.939
		// RX filter BW = 541.666667
		// Preamble count = 4
		// Address config = No address check
		// Whitening = false
		// Carrier frequency = 2432.999908
		// Device address = 0
		// TX power = 0
		// Manchester enable = false
		// CRC enable = true
		// Phase transition time = 0
		// Modulation format = MSK
		// Base frequency = 2432.999908
		// Modulated = true
		// Channel number = 0

		// Control Registers

		TRX_setSettingsReg(self, TRX_CR_IOCFG2,   0x2f); // hard wired to low
		TRX_setSettingsReg(self, TRX_CR_IOCFG1,   0x2e); // high impedance
		//TRX_setSettingsReg(self, TRX_CR_IOCFG0,   0x07); // asserts, when a packet was received with crc ok
		TRX_setSettingsReg(self, TRX_CR_IOCFG0,   0x01); // 27.72012: asserts, when the end of a packet is reached (and if threshold of RX-Fifo is less-or-equal than Rx-Bytes)
		TRX_setSettingsReg(self, TRX_CR_FIFOTHR,  0x0f); // set threshold to max. --> refer to (GDO0_CFG = 0x01)
		TRX_setSettingsReg(self, TRX_CR_SYNC1,    0xd3);
		TRX_setSettingsReg(self, TRX_CR_SYNC0,    0x91);
		TRX_setSettingsReg(self, TRX_CR_PKTLEN,   61); // 64-LenByte-RSSI-LQI = 61; // 27.6.2012 changed from 0xff
		TRX_setSettingsReg(self, TRX_CR_PKTCTRL1, 0x04|(1<<3)); // Append RSSI and LQI + CRC-Autoflush // TODO: remove Autoflush (also at cc1101)
		TRX_setSettingsReg(self, TRX_CR_PKTCTRL0, 0x05); // DON'T USE CC2400_EN, when using CRC-Autoflush!!
			// enable CC2400 support, so 0x07-mode for GD0 can be used
		TRX_setSettingsReg(self, TRX_CR_ADDR,     0x00);
		TRX_setSettingsReg(self, TRX_CR_CHANNR,   0x00);
		TRX_setSettingsReg(self, TRX_CR_FSCTRL1,  0x0a);
		TRX_setSettingsReg(self, TRX_CR_FSCTRL0,  0x00);
		TRX_setSettingsReg(self, TRX_CR_FREQ2,    0x5d);
		TRX_setSettingsReg(self, TRX_CR_FREQ1,    0x93);
		TRX_setSettingsReg(self, TRX_CR_FREQ0,    0xb1);
		TRX_setSettingsReg(self, TRX_CR_MDMCFG4,  0x2d);
		TRX_setSettingsReg(self, TRX_CR_MDMCFG3,  0x3b);
		TRX_setSettingsReg(self, TRX_CR_MDMCFG2,  0x73);
		TRX_setSettingsReg(self, TRX_CR_MDMCFG1,  0x22);
		TRX_setSettingsReg(self, TRX_CR_MDMCFG0,  0xf8);
		TRX_setSettingsReg(self, TRX_CR_DEVIATN,  0x00);
		TRX_setSettingsReg(self, TRX_CR_MCSM2,    0x07);
		TRX_setSettingsReg(self, TRX_CR_MCSM1,    (3<<4)|(3<<2)|(0<<0)); // 26.6.2012 changed from 0x30
			// Rx -> Rx, Tx -> IDLE; After received packet, stay in receive state,
			// after transmitted a packet, change to Idle for auto-calibrate (changed on 15.8.2012)
		TRX_setSettingsReg(self, TRX_CR_MCSM0,    0x28); // 15.8.2012 changed from 0x18 to 0x28 (auto calibrate)
		TRX_setSettingsReg(self, TRX_CR_FOCCFG,   0x1d);
		TRX_setSettingsReg(self, TRX_CR_BSCFG,    0x1c);
		TRX_setSettingsReg(self, TRX_CR_AGCCTRL2, 0xc7);
		TRX_setSettingsReg(self, TRX_CR_AGCCTRL1, 0x00);
		TRX_setSettingsReg(self, TRX_CR_AGCCTRL0, 0xb0);
		TRX_setSettingsReg(self, TRX_CR_WOREVT1,  0x87);
		TRX_setSettingsReg(self, TRX_CR_WOREVT0,  0x6b);
		TRX_setSettingsReg(self, TRX_CR_WORCTRL,  0xf8);
		TRX_setSettingsReg(self, TRX_CR_FREND1,   0xb6);
		TRX_setSettingsReg(self, TRX_CR_FREND0,   0x10);
		TRX_setSettingsReg(self, TRX_CR_FSCAL3,   0xea);
		TRX_setSettingsReg(self, TRX_CR_FSCAL2,   0x0a);
		TRX_setSettingsReg(self, TRX_CR_FSCAL1,   0x00);
		TRX_setSettingsReg(self, TRX_CR_FSCAL0,   0x11);
		TRX_setSettingsReg(self, TRX_CR_RCCTRL1,  0x41);
		TRX_setSettingsReg(self, TRX_CR_RCCTRL0,  0x00);
		TRX_setSettingsReg(self, TRX_CR_FSTEST,   0x59);
		TRX_setSettingsReg(self, TRX_CR_PTEST,    0x7f);
		TRX_setSettingsReg(self, TRX_CR_AGCTEST,  0x3f);
		TRX_setSettingsReg(self, TRX_CR_TEST2,    0x88);
		TRX_setSettingsReg(self, TRX_CR_TEST1,    0x31);
		TRX_setSettingsReg(self, TRX_CR_TEST0,    0x0b);
		}break;


		case TRX_CONFIG_CC1101:
		{
			strcat(self->name, " (CC1101)");
			self->settings.patable = 0x60; // =  0dBm // TODO: increase Power (optimum = 0dBm)!

			/*
			 *  433 MHz:
			 *  Power in dBm:	PA-Table value:
			 *  -30   			0x12
				-20   			0x0E
				-15   			0x1D
				-10   			0x34
				  0   			0x60
				  5   			0x84
				  7   			0xC8
				 10   			0xC0
			 */

			// Sync word qualifier mode = 30/32 sync word bits detected
			// CRC autoflush = false
			// Channel spacing = 199.951172
			// Data format = Normal mode
			// Data rate = 249.939
			// RX filter BW = 541.666667
			// PA ramping = false
			// Preamble count = 4
			// Address config = No address check
			// Whitening = false
			// Carrier frequency = 432.999817
			// Device address = 0
			// TX power = 0
			// Manchester enable = false
			// CRC enable = true
			// Deviation = 126.953125
			// Modulation format = GFSK
			// Base frequency = 432.999817
			// Modulated = true
			// Channel number = 0

			// Control Registers

			TRX_setSettingsReg(self, TRX_CR_IOCFG2,   0x1B|(1<<6)); // power amplifier - power down, active low = enable power amplifier, active high
						// don't use sleep mode!
						// self pin is directly connected to the PAC-Pin on the FT1100A-100 Module,
						// which is an active high enable for the TX-Amplifier
			TRX_setSettingsReg(self, TRX_CR_IOCFG1,   0x2e); // high impedance
			//RX_setSettingsReg(self, TRX_CR_IOCFG0,   0x07);
			TRX_setSettingsReg(self, TRX_CR_IOCFG0,   0x01); // 27.72012: asserts, when the end of a packet is reached (and if threshold of RX-Fifo is less-or-equal than Rx-Bytes)
				// deasserts, when rx-fifo is empty
			TRX_setSettingsReg(self, TRX_CR_FIFOTHR,  0x0f); // set threshold to max. --> refer to (GDO0_CFG = 0x01)
			TRX_setSettingsReg(self, TRX_CR_SYNC1,    0xd3);
			TRX_setSettingsReg(self, TRX_CR_SYNC0,    0x91);
			TRX_setSettingsReg(self, TRX_CR_PKTLEN,   61);
			TRX_setSettingsReg(self, TRX_CR_PKTCTRL1, 0x04|(1<<3)); // Append RSSI and LQI + CRC-Autoflush // TODO: remove Autoflush (also at cc2500)
			TRX_setSettingsReg(self, TRX_CR_PKTCTRL0, 0x05); // here isn't a cc2400 compatible mode
			TRX_setSettingsReg(self, TRX_CR_ADDR,     0x00);
			TRX_setSettingsReg(self, TRX_CR_CHANNR,   0x00);
			TRX_setSettingsReg(self, TRX_CR_FSCTRL1,  0x0c); // cc2500: 0x0a
			TRX_setSettingsReg(self, TRX_CR_FSCTRL0,  0x00);
			TRX_setSettingsReg(self, TRX_CR_FREQ2,    0x10);
			TRX_setSettingsReg(self, TRX_CR_FREQ1,    0xa7);
			TRX_setSettingsReg(self, TRX_CR_FREQ0,    0x62);
			TRX_setSettingsReg(self, TRX_CR_MDMCFG4,  0x2d);
			TRX_setSettingsReg(self, TRX_CR_MDMCFG3,  0x3b);
			TRX_setSettingsReg(self, TRX_CR_MDMCFG2,  0x13); // TODO: test, which Modulation is the best
			TRX_setSettingsReg(self, TRX_CR_MDMCFG1,  0x22);
			TRX_setSettingsReg(self, TRX_CR_MDMCFG0,  0xf8);
			TRX_setSettingsReg(self, TRX_CR_DEVIATN,  0x62); // cc2500: 0x00
			TRX_setSettingsReg(self, TRX_CR_MCSM2,    0x07);
			TRX_setSettingsReg(self, TRX_CR_MCSM1,    (3<<4)|(3<<2)|(0<<0)); // 3.7.2012 changed from 0x30
			// Rx -> Rx, Tx -> Rx; After received packet, stay in receive state,
			// after transmitted a packet, change to IDLE for auto-calibrate (changed on 15.8.2012)
			TRX_setSettingsReg(self, TRX_CR_MCSM0,    0x28); // 15.8.2012 changed from 0x18 to 0x28 (auto calibrate)
			TRX_setSettingsReg(self, TRX_CR_FOCCFG,   0x1d);
			TRX_setSettingsReg(self, TRX_CR_BSCFG,    0x1c);
			TRX_setSettingsReg(self, TRX_CR_AGCCTRL2, 0xc7);
			TRX_setSettingsReg(self, TRX_CR_AGCCTRL1, 0x00);
			TRX_setSettingsReg(self, TRX_CR_AGCCTRL0, 0xb0);
			TRX_setSettingsReg(self, TRX_CR_WOREVT1,  0x87);
			TRX_setSettingsReg(self, TRX_CR_WOREVT0,  0x6b);
			TRX_setSettingsReg(self, TRX_CR_WORCTRL,  0xfb); // cc2500: 0xf8, but wake on radio isn't used
			TRX_setSettingsReg(self, TRX_CR_FREND1,   0xb6);
			TRX_setSettingsReg(self, TRX_CR_FREND0,   0x10); // do not use power ramp-up and ramp-down
			TRX_setSettingsReg(self, TRX_CR_FSCAL3,   0xea);
			TRX_setSettingsReg(self, TRX_CR_FSCAL2,   0x2a); // cc2500: 0x0a
			TRX_setSettingsReg(self, TRX_CR_FSCAL1,   0x00);
			TRX_setSettingsReg(self, TRX_CR_FSCAL0,   0x1f); // cc2500: 0x11
			TRX_setSettingsReg(self, TRX_CR_RCCTRL1,  0x41);
			TRX_setSettingsReg(self, TRX_CR_RCCTRL0,  0x00);
			TRX_setSettingsReg(self, TRX_CR_FSTEST,   0x59);
			TRX_setSettingsReg(self, TRX_CR_PTEST,    0x7f);
			TRX_setSettingsReg(self, TRX_CR_AGCTEST,  0x3f);
			TRX_setSettingsReg(self, TRX_CR_TEST2,    0x88);
			TRX_setSettingsReg(self, TRX_CR_TEST1,    0x31);
			TRX_setSettingsReg(self, TRX_CR_TEST0,    0x09); // cc2500: 0x0b
		}break;
	}


	return self;
}

// reset trx-module (with pin toggling and software SPI)
// configure pins
// init SPI
//
TRX_Object* TRX_init(TRX_Object* self)
{

	resetChip(self); // Reset trx-module


	// configure pins:
	PIO_Configure(&(self->settings.pins.spiMISO), 1);
	PIO_Configure(&(self->settings.pins.spiMOSI), 1);
	PIO_Configure(&(self->settings.pins.spiSCK), 1);
	PIO_Configure(&(self->settings.pins.spiNPCS), 1);
	PIO_Configure(&(self->settings.pins.gd0), 1);
	PIO_Configure(&(self->settings.pins.gd2), 1);


	uint32_t status = self->settings.spiBase->SPI_SR; // self is needed, because the SR isn't writeable,
		// which is needed for the logical combination!
	if((status & AT91C_SPI_SPIENS) == 0) // when spi isn't enabled (=configured)
	{

		// workaround for SPI_Configure
			// it must not be called twice, because if so, the SPI_ConfigureNPCS of the first
			// configuration gets cleared by the software reset made in SPI_Configure.

		// configure SPI, and enable clock in the PMC:
		SPI_Configure(self->settings.spiBase, self->settings.spiID,
				1 << 0 | // master mode enable
				1 << 1 | // peripheral select (1=variable peripheral)
				0 << 2 | // peripheral chip select decode (0=direct connection)
				1 << 4 | // mode fault detection disable
				0 << 7 | // local loop back enable (0=off)
				0 << 16 | // peripheral chip select [0:3]; (see page 247 in the prelimainary)
				// not used for variable NPCS
				0 << 24); // delay between chip selects (default minimum 6 MCK cycles))

	}


	SPI_ConfigureNPCS(self->settings.spiBase, self->settings.npcs,
			0 << 0 | //clock polarity; // The inactive state value of SPCK is logic level zero.
			1 << 1 | // clock phase; //Data is captured on the leading edge of SPCK and changed on the following edge of SPCK.
			0 << 3 | // chip select remains active after transfer (0=false)
			(16 - 8) << 4 | // bits per transfer (8...16); value = num_bits-8
			BOARD_MCK / self->settings.spiBitrate << 8 | // SPCK bautdrate divider. BR=MCK/value
			0 << 16 | // delay before SPCK (default 1/2 SPCK cycle)
			1 << 24); // delay between consecutive transfers (for CC2500 at least 20ns are needed)
				// refer to the screenshot from the Hameg Oszilloscope SCR00075.bmp


	SPI_Enable(self->settings.spiBase);


	// it cannot be configured, when it is in power down mode!
//	TRX_writeCommandStrobe(self, TRX_CMD_SPWD, 0); // power down
//	wait(100);


	uint8_t n=0;
	for(n=0; n<TRX_NUM_CR; n++)
	{

		if(self->settings.regConfigured[n] && !self->settings.readonly[n])
		{
			TRX_writeConfigurationRegister(self, n, self->settings.configRegs[n]);
		}
	}
	TRX_writePaTable0(self, self->settings.patable); // set to full transmit power = 1dBm


	TRX_writeCommandStrobe(self, TRX_CMD_SCAL, 0); // clibrate

	wait(1000);

	return self;
}

// returns the read spi-word
static uint16_t spiWrite(TRX_Object* self, uint16_t txData)
{
	uint16_t rxData;
	SPI_Write(self->settings.spiBase, self->settings.npcs, txData);
	rxData = SPI_Read(self->settings.spiBase);

	self->statusByte.i = rxData >> 8; // the high byte of rxData is always the status byte

	return rxData;
}

// this can be used for reading the FIFO too
static uint8_t readStatusRegister(TRX_Object* self, TRX_STATUS_REGISTER regAddress)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	if(regAddress >= 0x30 && regAddress < 0x3F)
	{
		headerByte = READ_WRITEn_BIT | BURST_BIT; // Burst bit has the meaning of "Read Status Register" here
		headerByte |= regAddress & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	rxData = spiWrite(self, txData);
	return (rxData & 0xFF); // mask lower byte
}

// wrapper-function to workaround the bug on the SPI read synchronization issue
uint8_t TRX_readStatusRegister(TRX_Object* self, TRX_STATUS_REGISTER regAddress)
{
	uint8_t first = readStatusRegister(self, regAddress);
	uint8_t second = readStatusRegister(self, regAddress);
	uint8_t timeoutCounter = 5;
	while(first != second && timeoutCounter>0) // repeat, until we get a valid Status-Byte (Refer to errata of CC2500)
	{
		first = second;
		second = readStatusRegister(self, regAddress);
		timeoutCounter--;
	}

	if(timeoutCounter == 0 && first != second)
	{
		TRACE_ERROR("Cannot get valid status-register from trx in %s:%d\n", __FILE__, __LINE__);
	}

	return second;
}


// set read bit to get remaining bytes in the Rx-FIFO
// clear read-bit to get free bytes in the Tx-FIFO
void TRX_writeCommandStrobe(TRX_Object* self, TRX_COMMAND_STROBE regAddress, uint8_t RWnBit)
{
	uint8_t headerByte=0;
	uint16_t txData=0;
	uint8_t regAddr = regAddress; // typecast from int to uint8

	if(regAddr >= 0x30 && regAddr < 0x3F)
	{
		if(RWnBit)
		{
			headerByte = READ_WRITEn_BIT; // set read bit, no burst bit
			// not setting Burst bit has the meaning of "Write Command Strobe" here
			// setting the read-bit has the meaning of getting the RX-FIFO bytes remaining
		}
		headerByte |= regAddr & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	spiWrite(self, txData);
}

void TRX_writeConfigurationRegister(TRX_Object* self, TRX_CONFIGURATION_REGISTER regAddress, uint8_t value)
{
	uint8_t headerByte=0;
	uint16_t txData=0;

	if(regAddress >= 0x0 && regAddress <= 0x2E)
	{
		headerByte = 0; // no read-bit, no burst-bit
		headerByte |= regAddress & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	txData |= value;
	spiWrite(self, txData);
}

uint8_t TRX_readConfigurationRegister(TRX_Object* self, TRX_CONFIGURATION_REGISTER regAddress)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	if(regAddress >= 0x0 && regAddress <= 0x2E)
	{
		headerByte = READ_WRITEn_BIT; // set read-bit, no burst-bit
		headerByte |= regAddress & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	rxData = spiWrite(self, txData);
	return (rxData & 0xFF); // mask lower byte
}


// call PIO_InitializeInterrupts(0); before setupIrqHandlers()
void TRX_setupIrqHandlers(TRX_Object* self,
		void (*ISR_GD0)(const Pin* pPin),
		void (*ISR_GD2)(const Pin* pPin))
{
	// configure interrupts:
	// PIO_InitializeInterrupts(0);
	PIO_ConfigureIt(&(self->settings.pins.gd0), ISR_GD0);
	PIO_EnableIt(&(self->settings.pins.gd0));
	PIO_ConfigureIt(&(self->settings.pins.gd2), ISR_GD2);
	PIO_EnableIt(&(self->settings.pins.gd2));
}

// read from FIFO is exactly a read status register, but with the address 0x3F
// the nfifo-value in status byte stands for the RX-FIFO
uint8_t TRX_readFifo(TRX_Object* self)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	uint8_t regAddress = 0x3F;
	headerByte = READ_WRITEn_BIT | BURST_BIT; // Burst bit has the meaning of "Read Status Register" here
	// the read-bit has the meaning of getting the RX-FIFO remaining bytes
	headerByte |= regAddress & ADDRESS_MASK;

	txData = headerByte << 8; // shift to high-byte
	rxData = spiWrite(self, txData);
	return (rxData & 0xFF); // mask lower byte
}

// write to FIFO is like a write command strobe to address 0x3F, except, that a
// data byte is needed.
void TRX_writeFifo(TRX_Object* self, uint8_t data)
{
	uint8_t headerByte=0;
	uint16_t txData=0;

	uint8_t regAddress = 0x3F; // address of FIFO
	headerByte = 0; // no read bit, no burst bit
			// not setting Burst bit has the meaning of "Write Command Strobe" here
			// no read-bit has the meaning of getting the TX-FIFO free bytes available
	headerByte |= regAddress & ADDRESS_MASK;
	txData = headerByte << 8; // shift to high-byte
	txData |= data; // add data to the 16 bit txData
	spiWrite(self, txData);
}

// read from PA-Table is exactly a read status register, but with the address 0x3E
uint8_t TRX_readPaTable0(TRX_Object* self)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	uint8_t regAddress = 0x3E;
	headerByte = READ_WRITEn_BIT | BURST_BIT; // Burst bit has the meaning of "Read Status Register" here

	headerByte |= regAddress & ADDRESS_MASK;

	txData = headerByte << 8; // shift to high-byte
	rxData = spiWrite(self, txData);
	return (rxData & 0xFF); // mask lower byte
}

// write to PA-Table[0] is like a write command strobe to address 0x3E, except, that a
// data byte is needed.
void TRX_writePaTable0(TRX_Object* self, uint8_t data)
{
	uint8_t headerByte=0;
	uint16_t txData=0;

	uint8_t regAddress = 0x3E; // address of PA-Table
	headerByte = 0; // no read bit, no burst bit
			// not setting Burst bit has the meaning of "Write Command Strobe" here
			// no read-bit has the meaning of getting the TX-FIFO free bytes available
	headerByte |= regAddress & ADDRESS_MASK;
	txData = headerByte << 8; // shift to high-byte
	txData |= data; // add data to the 16 bit txData
	spiWrite(self, txData);
}


void TRX_setSettingsReg(TRX_Object* self, uint8_t regAddr, uint8_t regValue)
{
	self->settings.configRegs[regAddr] = regValue;
	self->settings.regConfigured[regAddr] = 1;
}


// compare register values in the settings with the registers in the transceiver chip.
// returns -1 on success, else the address of the register with differences
// only configured registers are compared.
int8_t TRX_compareRegisterValues(TRX_Object* self)
{
	for(int addr=0; addr < TRX_NUM_CR; addr++)
	{
		if(self->settings.regConfigured[addr])
		{
			uint8_t chipValue = TRX_readConfigurationRegister(self, addr);
			if(chipValue != self->settings.configRegs[addr])
			{
				return addr;
			}
		}
	}
	return -1;
}


//----------------------------------------------------------------------------------
// call before initialization of SPI
// 	needs the wait(), therefore it needs a working external time_100us!
//
//  DESCRIPTION:
//    Resets the chip using the procedure described in the datasheet.
//----------------------------------------------------------------------------------
static void resetChip(TRX_Object* self)
{
	/*
	 *    	HAL_SPI_CS_DEASSERT; --> high
			halMcuWaitUs(30);
			HAL_SPI_CS_ASSERT; --> low
			halMcuWaitUs(30);
			HAL_SPI_CS_DEASSERT; --> high
			halMcuWaitUs(45);

			// Send SRES command
			HAL_SPI_CS_ASSERT; --> low
			while(HAL_SPI_SOMI_VAL); // wait for MISO low
			HAL_SPI_TXBUF_SET(TRX_SRES); // write Reset Command Strobe
			HAL_SPI_WAIT_TXFIN;

			// Wait for chip to finish internal reset
			while (HAL_SPI_SOMI_VAL); // wait for MISO low again
			HAL_SPI_CS_DEASSERT;
	 */
	// TODO: configure pins for PIO
	Pin sck = self->settings.pins.spiSCK;
	Pin miso = self->settings.pins.spiMISO;
	Pin mosi = self->settings.pins.spiMOSI;
	Pin npcs = self->settings.pins.spiNPCS;


	sck.attribute = PIO_DEFAULT;
	miso.attribute = PIO_PULLUP | PIO_DEGLITCH;
	mosi.attribute = PIO_DEFAULT;
	npcs.attribute = PIO_DEFAULT;


	sck.type = PIO_OUTPUT_1;
	miso.type = PIO_INPUT;
	mosi.type = PIO_OUTPUT_0;
	npcs.type = PIO_OUTPUT_1;


	PIO_Configure(&sck, 1);
	PIO_Configure(&miso, 1);
	PIO_Configure(&mosi, 1);
	PIO_Configure(&npcs, 1);


	// todo: use following 2 lines according to the datasheet, but check SCK for the SRES Cmd
//	PIO_Set(&self->settings.pins.spiSCK);
//	PIO_Clear(&self->settings.pins.spiMOSI);

    // Toggle chip select signal
    PIO_Set(&npcs);//HAL_SPI_CS_DEASSERT;
    wait(30);
    PIO_Clear(&npcs);// HAL_SPI_CS_ASSERT;
    wait(30);
    PIO_Set(&npcs);//HAL_SPI_CS_DEASSERT;
    wait(45);


    // Send SRES command
	PIO_Clear(&sck); // SPI Word starts with low level on SCK
    wait(30);
    PIO_Clear(&npcs);// HAL_SPI_CS_ASSERT; --> low
    while(PIO_Get(&miso))
    	KICK_DOG; // while(HAL_SPI_SOMI_VAL);// wait for MISO low


    // clock out SRES by software:
    // The data on the MOSI will be sampled on the rising edge of SCLK
    // -- SCLk is allready high --> first clear SCLK and then set Pin to MSB, before tie SCLK to low!


    uint8_t data = TRX_CMD_SRES;
    data |= 0; // No Burst bit; R/Wn-Bit doesn't matter here
    uint8_t mask = (1<<7); // MSB first
    while(mask)
    {
    	KICK_DOG;
    	if(data & mask)
    	{
    		//PIO_Set(&self->settings.pins.spiMOSI);
     		PIO_Set(&mosi);
    	}
    	else
    	{
    		//PIO_Clear(&self->settings.pins.spiMOSI);
    		PIO_Clear(&mosi);
    	}
        wait(30);
        PIO_Set(&sck); // write bit to trx-module
        wait(30);
    	PIO_Clear(&sck);

    	mask >>= 1;
    }


    // Wait for chip to finish internal reset
    while(PIO_Get(&miso))
    	KICK_DOG; // wait for MISO-Pin to go low again

    PIO_Set(&npcs); //HAL_SPI_CS_DEASSERT;// --> high

}


// wait for the module to get in a state
void TRX_waitForState(TRX_Object* self, TRX_STATE state)
{
	TRACE_DEBUG("TRX_waitForState()\n");
	/* Coming from TRX_Setup() A NOP COMMAND STROBE IS NEEDED BEFORE READING THE STATUS BYTE */
	uint32_t tStart = time_100us;
	do
	{
		TRX_getValidStatusByte(self);
	}while(self->statusByte.sb.state != state && time_100us < tStart + WAIT_FOR_STATE_TIMEOUT);
	if(time_100us >= tStart + WAIT_FOR_STATE_TIMEOUT)
	{
		TRACE_FATAL("TRX: %s\n"
				"Waiting for state == %d timed out\n"
				"Current state = %d\n",self->name, state, self->statusByte.sb.state);
	}
	TRACE_DEBUG("end of TRX_waitForState()\n");
}

// repeat NOP command strobes, until the same status byte can be read twice
uint8_t TRX_getValidStatusByte(TRX_Object* self)
{
	TRACE_DEBUG("TRX_getValidStatusByte()\n");
	TRX_writeCommandStrobe(self, TRX_CMD_SNOP, 0);
	uint8_t firstSt = self->statusByte.i;
	TRX_writeCommandStrobe(self, TRX_CMD_SNOP, 0);
	uint8_t secondSt = self->statusByte.i;
	int8_t timeoutCounter = 5;
	while(firstSt != secondSt && timeoutCounter>0) // repeat, until we get a valid Status-Byte (Refer to errata of CC2500)
	{
		firstSt = secondSt;
		TRX_writeCommandStrobe(self, TRX_CMD_SNOP, 0);
		secondSt = self->statusByte.i;
		timeoutCounter--;
	}

	TRACE_DEBUG("timeoutCounter = %d\n", timeoutCounter);

	if(timeoutCounter == 0 && firstSt != secondSt)
	{
		TRACE_ERROR("Cannot get valid status-byte from trx in %s:%d\n", __FILE__, __LINE__);
	}

	TRACE_DEBUG("end of TRX_getValidStatusByte()\n");
	return secondSt;
}

// return value: decimal fixed point: -245 = -24.5 dBm
// the returned value can be interpreted as a value of the unit cBm ("centi Bel milli-Watt")
int16_t TRX_num2rssi(uint8_t num)
{
	int16_t rssi = (int16_t) num * 10;
	const int16_t rssi_offset = 71 * 10;
	if (rssi >= 128 * 10)
	{
		rssi = (rssi - 256 * 10) / 2 - rssi_offset;
	}
	else
	{
		rssi = rssi / 2 - rssi_offset;
	}
	return rssi;
}

void wait(uint32_t microSeconds)
{
	uint32_t t0 = time_100us;
	while(time_100us < (t0 + microSeconds/100 + 1)) // wait for PIT to count the time up
	{
		KICK_DOG;
	}
}
