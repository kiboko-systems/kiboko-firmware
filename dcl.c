
/*
 * Module
 * Debug Command Line
 *
 * Karl Zeilhofer
 * 28.6.2012
 *
 */

#include "dcl.h"

#include "defs.h"
#include "settings.h"
#include "rfswshared.h"


// TODO: Important
/*
 * A und B müssen einen separaten Zustand bekommen dclState muss getrennt für A und B verwaltet wereden.
 * Dazü würde ein Feld "number" im TRX objekt sinn machen, um ein Status-Array indizieren zu können mit
 * hilfe von actTrx zB.
 *
 * Button Status dem GD0-Handler mitübergeben, um die lokale definition zu vermeiden.
 */

typedef uint32_t TIME_T;


static TRX_Object* trxA = NULL; // RF-Module A
static TRX_Object* trxB = NULL; // RF-Module B
static TRX_Object* actTrx = NULL;

static Pin pinButton = PIN_PUSHBUTTON_1; // use static like "private" here, to avoid conflicts with main

static uint8_t readNumber(const unsigned char* str, int i);
static int findSpace(const unsigned char* str, int iStart);
static void writeDummyPacket(TRX_Object* rfObj);

enum {St_default, St_dummyTransmitter, St_printRssi, St_armedRecorder, St_recorder
} dclState = St_default; // global state of program

typedef struct
{
	uint32_t t;
	int16_t rssi;
	uint8_t button;
}Record;

//#define B_LEN (6000) // (max, needs 6000*7 Bytes = 42kB)lasts for 60s @ 100Hz
#define B_LEN (100) // lasts for one second

typedef struct
{
	uint32_t index;
	Record buffer[B_LEN];
}Recorder;

volatile Recorder rec;


// Toggle LED on received/sent packet


void DCL_Init(TRX_Object* pTrxA, TRX_Object* pTrxB)
{
	trxA = pTrxA;
	trxB = pTrxB;

	actTrx = trxA; // init actRf
	rec.index = 0;

}

DCL_Command DCL_FeedWithChar(char c)
{
	static unsigned char in0[100]={0}; // needed for a. and b. prefixes
	static unsigned char* in=in0;
	DCL_Command cmd = DCL_cNone;

	if(trxA==NULL && trxB==NULL)
	{
		TRACE_WARNING("trxA and trxB are NULL in line %d in %s\n", __LINE__, __FILE__);
		return cmd;
	}

	static uint8_t n=0; // character counter
	uint8_t ans; // answer of commands

	in[n++]=c;
	if(in[n-1]==(unsigned char)'\n') // end of command
	{
		in[n]=0; // finalize string
		printf("%s", in); // echo
		if(strncmp((char*)in, "a.", 2)==0)
		{
			actTrx = trxA;
			in = in0+2;
		}else if(strncmp((char*)in, "b.", 2)==0)
		{
			actTrx = trxB;
			in = in0+2;
		}

		if(actTrx == NULL)
		{
			TRACE_WARNING("actTrx is NULL in line %d in %s\n", __LINE__, __FILE__);
		}

		if(strncmp((char*)in, "help", 4)==0)
		{
			cmd = DCL_cHelp;
			printf("Valid commands:\n");
			printf("use prefix \"a.\" for 2.4GHz or \"b.\" for 433MHz to select the module\n");
			printf("\n");
			printf("  System:\n");
			printf("    clear                   // delete config in flash\n");
			printf("    start                   // start processing loop of RFSW\n");
			printf("    stop                    // leave processing loop of RFSW\n");
			printf("    printrf                 // print all informations about rf-module(s)\n");
			printf("    volt                    // print battery voltages\n");
			printf("    reboot                  // halt and wait for watchdog-reset\n");
			printf("  Registers:\n");
			printf("    st                      // read status byte, and decode it\n");
			printf("    setreg <addr> <value>   // write configuration register\n");
			printf("    getreg <addr>           // read configuration register\n");
			printf("    getsreg <addr>          // read status register\n");
			printf("    setpa <value>           // set value in PA-Table[0]\n");
			printf("    getpa                   // read PA-Table[0]\n");
			printf("    rssi                    // read RSSI value\n");
			printf("\n");
			printf("  RF-Commands:\n");
			printf("    cmd <cmd-strobe>        // write command strobe to RF-Module\n");
			printf("    rfreset        			// reset and reinitialize RF-Module\n");
			// TODO: printf("    ledon                 // turn power-LED on\n");
			// TODO: printf("    ledoff                // turn power-LED off\n");
			printf("\n");
			printf("  FIFO:\n");
			printf("    fifowrite <value>       // write one byte to transmit FIFO\n");
			printf("    fiforead                // read one byte of receive FIFO\n");
			printf("    getfifo                 // read all available bytes from FIFO\n");
			printf("    fifowritetest           // write one packet with dummy data\n");
			printf("    flushrx                 // flush receive FIFO\n");
			printf("    flushtx                 // flush transmit FIFO\n");
			printf("\n");
			printf("  Utilities\n");
			printf("    num2rssi <number>       // converts a number to an RSSI value\n");
			printf("    checkregs               // compare all registers in settings and on the chip\n");
			printf("    name                    // print name of the active module\n");
			printf("    settime <hour> <minute> <second>    // set base-time on the time-base module\n");
			printf("        write e.g. 17 37 46 for 17h37m46s\n");
			printf("    gettime       			// print current time\n");
			printf("    debug       			// print various debug outputs\n");
						// TODO: printf("    setboard <value>      // 1=Zeitbasis, 21=Station 1, 22=Station 2, 31=Zille 1, 32=Zille 2\n");
			printf("\n");
			printf("  Handling Operating Modes\n");
			printf("    dummytx                 // send permanently dummy packets\n");
			printf("    printrssi               // print RSSI value of every received packet\n");
			printf("    record                  // record rssi values into buffer (60s)\n");
			printf("    break                   // enter default state\n");
			printf("    getbuffer               // print recorded buffer content\n");
			printf("    matrixteststart         // start Matrix-Test\n");
			printf("    matrixteststop          // stop Matrix-Test\n");
			printf("    matrixrainstart         // start Matrix-Pixel-Rain\n");
			printf("    matrixrainstop          // stop Matrix-Pixel-Rain\n");

		}
		else if(strncmp((char*)in, "setreg ", 7)==0)
		{
			cmd = DCL_cSetreg;
			uint8_t i1, i2; // parameter indices
			uint8_t p1,p2; // decoded parameters
			i1 = 7;
			p1 = readNumber(in, i1);
			i2 = findSpace(in, 7);
			i2++; // skip space
			if(i2>=0)
			{
				p2 = readNumber(in, i2);
			}

			TRX_writeConfigurationRegister(actTrx, p1, p2);
			//printf("TRX_writeConfigurationRegister(actRf, 0x%02x, 0x%02x);\n", p1, p2);
			printf("    status = 0x%02x\n", actTrx->statusByte.i);

		}else if(strncmp((char*)in, "getreg ", 7)==0)
		{
			cmd = DCL_cGetreg;
			uint8_t i1; // parameter indices
			uint8_t p1; // decoded parameters
			i1 = 7;
			p1 = readNumber(in, i1);

			ans = TRX_readConfigurationRegister(actTrx, p1);
			//printf("TRX_readConfigurationRegister(actRf, 0x%02x);\n", p1);
			printf("    ans = 0x%02x\n", ans);
			printf("    status = 0x%02x\n", actTrx->statusByte.i);
		}else if(strncmp((char*)in, "cmd ", 4)==0)
		{
			cmd = DCL_cCmd;
			uint8_t i1; // parameter indices
			uint8_t p1; // decoded parameters

			i1 = 4;
			p1 = readNumber(in, i1); // command strobe address

			TRX_writeCommandStrobe(actTrx, p1, 0);
			//printf("TRX_writeCommandStrobe(actRf, 0x%02x, 0);\n", p1);
			printf("    status = 0x%02x\n", actTrx->statusByte.i);
		}else if(strncmp((char*)in, "rfreset", 7)==0)
		{
			/*
			 * Only GD0-Pin IRQ has to be disabled and enabled again!
			 * GD2-Pin is not used!
			 */
			cmd = DCL_cRfreset;
			printf("    Disabling GD0 IRQ of RF-Module (%s)...\n", actTrx->name);
			PIO_DisableIt(&(actTrx->settings.pins.gd0));
			printf("    Resetting RF-Module (%s)...\n", actTrx->name);
			TRX_init(actTrx);
			printf("    Enabling GD0 IRQ of RF-Module (%s)...\n", actTrx->name);
			PIO_EnableIt(&(actTrx->settings.pins.gd0));
			printf("    Reseted RF-Module (%s)\n", actTrx->name);
		}else if(strncmp((char*)in, "getsreg ", 8)==0)
		{
			cmd = DCL_cGetreg;
			uint8_t i1; // parameter indices
			uint8_t p1; // decoded parameters

			i1 = 8;
			p1 = readNumber(in, i1);

			ans = TRX_readStatusRegister(actTrx, p1);
			//printf("TRX_readStatusRegister(actRf, 0x%02x);\n", p1);
			printf("    ans = 0x%02x\n", ans);
			printf("    status = 0x%02x\n", actTrx->statusByte.i);
		}else if(strncmp((char*)in, "fifowrite ", 10)==0)
		{
			cmd = DCL_cFifowrite;
			uint8_t i1; // parameter indices
			uint8_t p1; // decoded parameters

			i1 = 10;
			p1 = readNumber(in, i1);

			TRX_writeFifo(actTrx, p1);
			//printf("TRX_writeFifo(actRf, 0x%02x);\n", p1);
			printf("    status = 0x%02x\n", actTrx->statusByte.i);
			printf("    FIFO bytes left = %d\n", (actTrx->statusByte.i)&0x0F);
		}else if(strncmp((char*)in, "fiforead", 8)==0)
		{
			cmd = DCL_cFiforead;
			ans = TRX_readFifo(actTrx);
			//printf("TRX_writeFifo(actRf);\n");
			printf("    ans = 0x%02x\n", ans);
			printf("    status = 0x%02x\n", actTrx->statusByte.i);
			printf("    FIFO bytes available = %d\n", (actTrx->statusByte.i)&0x0F);
		}else if(strncmp((char*)in, "start", 3)==0)
		{ // TODO: test this command
			cmd = DCL_cStart;
			printf("    Processing loop started\n");
		}else if(strncmp((char*)in, "stop", 4)==0)
		{ // TODO: test this command
			cmd = DCL_cStop;
			printf("    Processing loop stopped\n");
		}else if(strncmp((char*)in, "st", 2)==0)
		{
			cmd = DCL_cSt;
			uint8_t state;
			uint8_t bytes;
			printf("    %s\n", actTrx->name);

			bytes = TRX_readStatusRegister(actTrx, TRX_SR_RXBYTES);
			if(bytes&(1<<7))
			{
				printf("    ### Rx Overflow ###\n");
			}
			bytes &= ~(1<<7);
			printf("    Rx-FIFO has %d bytes\n", bytes);

			bytes = TRX_readStatusRegister(actTrx, TRX_SR_TXBYTES);
			if(bytes&(1<<7))
			{
				printf("    ### Tx Underflow ###\n");
			}
			bytes &= ~(1<<7);
			printf("    Tx-FIFO has %d bytes\n", bytes);

			state = ((actTrx->statusByte.i)>>4) & 0x07;
			printf("    state: ");
			switch(state)
			{
				case 0: printf("IDLE\n");break;
				case 1: printf("Rx Mode\n");break;
				case 2: printf("Tx Mode\n");break;
				case 3: printf("Random Tx\n");break;
				case 4: printf("Calibrate\n");break;
				case 5: printf("Settling\n");break;
				case 6: printf("RxFIFO overflow\n");break;
				case 7: printf("TxFIFO underflow\n");break;
			}

		}else if(strncmp((char*)in, "name", 4)==0)
		{
			cmd = DCL_cName;
			printf("    %s\n", actTrx->name);
		}else if(strncmp((char*)in, "rssi", 4)==0)
		{
			cmd = DCL_cRssi;
			ans = TRX_readStatusRegister(actTrx, TRX_SR_RSSI);
			int16_t rssi = DCL_num2rssi(ans);
			printf("    RSSI = %d dBm\n", rssi);
		}else if(strncmp((char*)in, "flushrx", 7)==0)
		{
			cmd = DCL_cFlushrx;
			TRX_writeCommandStrobe(actTrx, (uint8_t)0x3a, 0);
			printf("    Rx-Buffer flushed\n");
		}else if(strncmp((char*)in, "flushtx", 7)==0)
		{
			cmd = DCL_cFlushtx;
			TRX_writeCommandStrobe(actTrx, TRX_CMD_SFTX, 0);
			printf("    Tx-Buffer flushed\n");
		}else if(strncmp((char*)in, "num2rssi ", 9)==0)
		{
			cmd = DCL_cNum2rssi;
			uint8_t i1; // parameter indices
			uint8_t p1; // decoded parameters

			i1 = 9;
			p1 = readNumber(in, i1);
			int16_t rssi = DCL_num2rssi(p1);
			printf("    RSSI = %d dBm\n", rssi);
		}else if(strncmp((char*)in, "getpa", 5)==0)
		{
			cmd = DCL_cGetpa;
			ans = TRX_readPaTable0(actTrx);
			printf("    PA-Table[0] = 0x%02x\n",ans);
		}else if(strncmp((char*)in, "setpa ", 6)==0)
		{
			cmd = DCL_cSetpa;
			uint8_t i1; // parameter indices
			uint8_t p1; // decoded parameters

			i1 = 6;
			p1 = readNumber(in, i1);

			TRX_writePaTable0(actTrx, p1);
			printf("    status = 0x%02x\n", actTrx->statusByte.i);
		}else if(strncmp((char*)in, "fifowritetest", 13)==0)
		{
			cmd = DCL_cFifowritetest;
			TRX_writeCommandStrobe(actTrx, TRX_CMD_SFTX, 0); // flush buffer
			TRX_writeCommandStrobe(actTrx, TRX_CMD_SNOP, 0); // NOP, to the free bytes left
			writeDummyPacket(actTrx);
		}else if(strncmp((char*)in, "getfifo", 7)==0)
		{
			cmd = DCL_cGetfifo;
			int n=0;
			printf("    ans = \n");
			TRX_writeCommandStrobe(actTrx, TRX_CMD_SNOP, 1);
			while(((actTrx->statusByte.i)&0x0F) > 1)
			{
				ans = TRX_readFifo(actTrx);
				printf("0x%02x ", ans);
				n++;
				if(n%8 == 0) // linebreak after 8 bytes
				{
					printf("\n");
				}
			}
			printf("\n");
			printf("    status = 0x%02x\n", actTrx->statusByte.i);
		}else if(strncmp((char*)in, "checkregs", 9)==0)
		{
			cmd = DCL_cCheckregs;
			int8_t ret = TRX_compareRegisterValues(actTrx);
			if(ret == -1)
			{
				printf("    Checked successfully!\n");
			}else
			{
				printf("    Check failed on address: 0x%02x!\n",ret);
			}

			printf("    status = 0x%02x\n", actTrx->statusByte.i);
		}else if(strncmp((char*)in, "dummytx", 7)==0)
		{
			cmd = DCL_cDummytx;
			dclState = St_dummyTransmitter;
			writeDummyPacket(actTrx); // trigger, next is handled by IRQ
			TRX_writeCommandStrobe(actTrx, TRX_CMD_STX, 0); // start transmit
		}else if(strncmp((char*)in, "printrssi", 9)==0)
		{
			cmd = DCL_cPrintrssi;
			dclState = St_printRssi;
		}else if(strncmp((char*)in, "break", 5)==0)
		{
			cmd = DCL_cBreak;
			TRX_writeCommandStrobe(actTrx, TRX_CMD_SIDLE, 0); // stop tx/rx
			dclState = St_default;
			LED_Clear(LED_DS1);
			PWMC_DisableChannel(0);
		}else if(strncmp((char*)in, "record", 6)==0)
		{
			cmd = DCL_cRecord;
			printf("   Push Button on the Board to start recording.\n");
			printf("   Note: Buffersize = %d Samples\n", B_LEN);
			dclState = St_armedRecorder;
		}else if(strncmp((char*)in, "getbuffer", 9)==0)
		{
			cmd = DCL_cGetbuffer;
			TRX_writeCommandStrobe(actTrx, TRX_CMD_SIDLE, 0); // stop tx/rx
			dclState = St_default;
			printf("%% t in 100us, rssi in 0.1 dBm, Button 0/1\n");
			printf("%% Number of samples: %d\n", (int)rec.index);
			for(int n=0; n<rec.index; n++)
			{
				printf("%u %d %d\n", (unsigned int)rec.buffer[n].t, (int)rec.buffer[n].rssi, (int)rec.buffer[n].button);
			}
		}else if(strncmp((char*)in, "settime ", 8)==0)
		{
			cmd = DCL_cSettime;
			uint8_t i1, i2, i3; // parameter indices
			uint8_t h,m,s; // decoded parameters
			i1 = 8;
			h = readNumber(in, i1);
			i2 = findSpace(in, i1);
			i2++; // skip space
			if(i2>=0)
			{
				m = readNumber(in, i2);
				i3 = findSpace(in, i2);
				i3++; // skip space
				if(i3>=0)
				{
					s = readNumber(in, i3);
				}
			}
			TIME_T tmp = h*10000*3600 + m*10000*60 + s*10000;
			time_100us = tmp;
		}else if(strncmp((char*)in, "gettime", 7)==0)
		{
			cmd = DCL_cGettime;
			uint8_t h,m,s;
			TIME_T t = time_100us;
			TIME_T t0 = t;

			h =  t/10000/3600;
			t = t-h*10000*3600;
			m = t/10000/60;
			t = t-m*10000*60;
			s = t/10000;
			t = t-s*10000; // t holds sub-seconds in 100us-units
			printf("    time = %02d:%02d:%02d.%04d (%u)\n", (int)h, (int)m, (int)s, (int)t, (unsigned int)t0);
		}else if(strncmp((char*)in, "debug", 5)==0)
		{
			cmd = DCL_cDebug; // implement some outputs in the main programm
			printf("    Printing Debug Outputs\n");
		}else if(strncmp((char*)in, "clear", 5)==0)
		{
			cmd = DCL_cClear;
			printf("    Clear Config in Flash...");
			SETTINGS zeroSet;
			zeroSet.version = 0;
			zeroSet.flashKey = 0;
			SET_writeToFlash(&zeroSet);
			printf("[OK]\n");
		}else if(strncmp((char*)in, "printrf", 7)==0)
		{
			cmd = DCL_cPrintrf;
		}else if(strncmp((char*)in, "volt", 4)==0)
		{
			cmd = DCL_cVolt;
		}else if(strncmp((char*)in, "reboot", 6)==0)
		{
			cmd = DCL_cReboot;
			while(1); // wait for watchdog
		}else if(strncmp((char*)in, "matrixteststart", 15)==0)
		{
			cmd = DCL_cMatrixTestStart;
		}else if(strncmp((char*)in, "matrixteststop", 14)==0)
		{
			cmd = DCL_cMatrixTestStop;
		}else if(strncmp((char*)in, "matrixrainstart", 15)==0)
		{
			cmd = DCL_cMatrixRainStart;
		}else if(strncmp((char*)in, "matrixrainstop", 14)==0)
		{
			cmd = DCL_cMatrixRainStop;
		}else if(strncmp((char*)in, "printpf", 7)==0)
		{
			cmd = DCL_cPrintpf; // print photo filter (current Photo-Detector Filter settings)
		}else // no command found
		{
			printf("Unknown Command!\n");
		}
		n = 0;
		in = in0; // reset 'in'-pointer to the start of buffer
	} // end if

	if(n>=100){
		printf("Input Buffer exceeded. Reset Input Buffer\n");
		n=0;
	}

	return cmd;
}


// read number, coded in decimal or 0xHEX ends with space-character
// number starts at index i in the string str.
static uint8_t readNumber(const unsigned char* str, int i)
{
	uint8_t num=0;
	uint8_t d=0; //number of digits

	if(strncmp((char*)(&str[i]), "0x", 2)==0) // hex number
	{
		i+=2;
		while(d<2 && str[i] >= '0' && toupper((int)str[i]) <= 'F')
		{
			num*=(uint8_t) 16;
			if(isdigit(toupper(str[i])))
			{
				num += toupper(str[i])-'0';
			}else
			{
				num += toupper(str[i])-'A'+10;
			}
			d++;
			i++;
		}
	}else // decimal number
	{
		while(d<3 && str[i] >= '0' && str[i] <= '9')
		{
			num*=10;
			num += (uint8_t)(str[i]-'0');
			d++;
			i++;
		}
	}

	return num;
}

// returns the index of the next occurrence of ' ' in the string
// if none found, -1 is returned.
static int findSpace(const unsigned char* str, int iStart)
{
	while(str[iStart] != 0)
	{
		if(str[iStart]==' ')
		{
			return iStart;
		}
		iStart++;
	}
	return -1;
}

// return value: decimal fixed point: -245 = -24.5 dBm
int16_t DCL_num2rssi(uint8_t num)
{
	int16_t rssi = (int16_t)num*10;
	const int16_t rssi_offset = 71*10;
	if(rssi >= 128*10)
	{
		rssi = (rssi-256*10)/2 - rssi_offset;
	}else
	{
		rssi = rssi/2 - rssi_offset;
	}
	return rssi;
}


static void writeDummyPacket(TRX_Object* rfObj)
{
	TRX_writeFifo(rfObj, 10); // set packet length
	for(int n=0; n<10; n++)
	{
		TRX_writeFifo(rfObj, n);
	}
}



void DCL_Gd0ChangedHandler(TRX_Object* pRf)
{
	if(pRf == NULL)
	{
		return;
	}
	TRACE_DEBUG("%s: GD0 changed\n", pRf->name);
	if(!PIO_Get(&(pRf->settings.pins.gd0))) // on falling edge of GD0:
	{
		//printf("GD0: 1->0\n");
		LED_Toggle(LED_DS1);
		//TRX_writeCommandStrobe(pRfA, TRX_CMD_SFRX, 0); // flush RX

		switch(dclState)
		{
			case St_default:
				// nothing to to yet.
			break;
			case St_dummyTransmitter:
			{
				static uint32_t lastTxTime_100us = 0;

				while(time_100us <= lastTxTime_100us + 1000); // limit to 10Hz tx
				lastTxTime_100us = time_100us;
				writeDummyPacket(pRf);
				TRX_writeCommandStrobe(pRf, TRX_CMD_STX, 0); // start transmit

			}break;
			case St_printRssi:
			{
				int n = TRX_readFifo(pRf); // get packet length
				for(;n>0; n--)
				{
					TRX_readFifo(pRf); // read dummy data
				}
				uint8_t rssi = TRX_readFifo(pRf); // read rssi value
				//uint8_t lqi_crc = TRX_readFifo(pRf); // read lqi value
				TRX_writeCommandStrobe(pRf, TRX_CMD_SFRX, 0); // flush rx buffer
				int16_t rssi_dBm = DCL_num2rssi(rssi);
				printf("%d %u\n", (int)rssi_dBm, (unsigned int)time_100us);
				TRX_writeCommandStrobe(pRf, TRX_CMD_SRX,0); // enter receive mode again!
			}break;
			case St_recorder:
			{
				int n = TRX_readFifo(pRf); // get packet length
				for(;n>0; n--)
				{
					TRX_readFifo(pRf); // read dummy data
				}
				uint8_t rssi = TRX_readFifo(pRf); // read rssi value
				TRX_writeCommandStrobe(pRf, TRX_CMD_SFRX, 0); // flush rx buffer
				rec.buffer[rec.index].rssi = DCL_num2rssi(rssi);
				rec.buffer[rec.index].t = time_100us;
				rec.buffer[rec.index].button = PIO_Get(&pinButton) ? 0:1;
				rec.index++;

				if(rec.index >= B_LEN)
				{
					dclState = St_default;
					LED_Clear(LED_DS1);
					printf("    Buffer full");
				}else
				{
					TRX_writeCommandStrobe(pRf, TRX_CMD_SRX,0); // enter receive mode again!
				}
			}break;
			case St_armedRecorder:
			{
				// nothing to do here
			}break;
		}
	}
}


void DCL_ButtonChangedHandler(bool isPressed)
{
	if(isPressed) // if pressed
	{
		switch(dclState)
		{
			case St_armedRecorder:
			{
				printf("    Start recording...\n");
				rec.index = 0;
				TRX_writeCommandStrobe(actTrx, TRX_CMD_SRX, 0); // start receive
				dclState = St_recorder;
			}break;
			default:
			{
				//nothing to do
			}break;
		}
	}
}

