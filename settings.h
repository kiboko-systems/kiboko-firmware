/*
 * settings.h
 *
 *  Created on: 6.7.2012
 *      Author: Karl Zeilhofer
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include "defs.h"
#include <memories/flash/flashd.h>
#include <efc/efc.h>
#include <stdbool.h>


#define SETTINGS_VERSION (2)
#define SETTINGS_FLASH_KEY (uint32_t)(1234567890) // any constant random number

#define FLASH_BLOCK_SIZE (4*AT91C_IFLASH_PAGE_SIZE) // block of 1024 bytes (= 4 pages each 256 bytes)
#define SETTINGS_FLASH_ADDR (AT91C_IFLASH + AT91C_IFLASH_SIZE - FLASH_BLOCK_SIZE) // using last four pages each 256 bytes
#define PULSE_SETTINGS_FLASH_ADDR (AT91C_IFLASH + AT91C_IFLASH_SIZE-(2*1024*16))

typedef enum{SET_BT_TimeBase, SET_BT_TriggerStation, SET_BT_BoatBox, SET_BT_MatrixBoard} SET_BOARD_TYPE;


// CAUTION: always use 4-byte alignment
// (or even 8-byte alignment if this structure has to be transmitted to a modern PC,
// which is a 64-bit system)

// sizeof(SETTINGS) must be <=254

typedef struct structSettings
{
	int32_t version; // version number of settings
	SET_BOARD_TYPE boardType;
	uint8_t boardNumber; // 1 - 25
	uint8_t address_433M;
	uint8_t address_2G4;

	uint8_t ALIGN_TO_FOUR; // dummy entry, to align to four bytes.

	uint32_t flashKey; // check bytes for successful written bytes to flash
} SETTINGS;

SETTINGS* SET_loadFromFlash(SETTINGS* set, const SETTINGS* defaultSettings);
void SET_writeToFlash(SETTINGS* set);
void SET_print(SETTINGS* set);
uint8_t SET_compare(SETTINGS* set1, SETTINGS* set2);

#endif /* SETTINGS_H_ */
