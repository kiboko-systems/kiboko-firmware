
#include <board.h>
#include "softpll.h"
#include "globals.h"

// Implementation based on Delta-Sigma Digital to Digital Converter
// please see https://gitlab.com/KarlZeilhofer/DeltaSigmaDDC/tree/master

int32_t DRef_minus = -10000;
int32_t DRef_plus = +10000;
int32_t output=0; // +1, 0, -1; unit in 1/3 micro seconds
int32_t freqOffset_thirdPPM=0; // the input corresponds to the frequency offset in 1/3 ppm
	// limited to +-200ppm, --> +-600
int32_t sum=0;

TIME_T lastUpdate = 0;


// this must be called in every 100us PIT-ISR
// it returns +1, 0 or -1 (unit: 0.333 us)
// add this returned value to the nominal PIT_INTERVAL
// in the PIT-ISR for compensation.
int32_t SoftPLL_step()
{
	int32_t diff;

	if(output==1)
	{
		diff = freqOffset_thirdPPM-DRef_plus;
	}else if(output==-1)
	{
		diff = freqOffset_thirdPPM-DRef_minus;
	}else
	{
		diff = freqOffset_thirdPPM;
	}

	sum = sum + diff; // here is the time step

	if(sum > DRef_plus){
		output = 1;
	}else if(sum < DRef_minus){
		output = -1;
	}else{
		output = 0;
	}

	return output;
}


// delta_thirdMicros = local time - reference time
// --> positive, if local time is too fast
// --> negative, if local time is too slow
void SoftPLL_updateFrequencyOffset(int32_t delta_thirdMicros)
{
	TIME_T now = time_100us;

	int32_t updatePeriod = now-lastUpdate;

	if(updatePeriod < 20000) // 2.0 seconds
	{

		// limit step-size to prevent integer overflow
		if(delta_thirdMicros < -200e6)
			delta_thirdMicros = -200e6;
		if(delta_thirdMicros > +200e6)
			delta_thirdMicros = +200e6;


		// PLL-controller

		static int32_t integral=0;
		{
			integral += delta_thirdMicros;
			freqOffset_thirdPPM = (integral)/2 + 5*delta_thirdMicros;

			// limit Freuquency Offset to max. +-100ppm
			if(freqOffset_thirdPPM > 600)
				freqOffset_thirdPPM = 600;
			if(freqOffset_thirdPPM < -600)
				freqOffset_thirdPPM = -600;
		}

		lastUpdate = now;

	}else{
		lastUpdate = now;
	}
}
