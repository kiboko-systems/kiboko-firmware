/*
 * matrixbox.c
 *
 *  Created on: 09.08.2013
 *      Author: Karl Zeilhofer
 *
 * Raining-Pixel-Mode:
 * 		start an einer beliebigen stelle im bild
 * 		regnet für eine beliebige länge zwischen 20 und 80% der höhe
 * 		regengeschwindigkeit ist konstant.
 */

#include "matrixboard.h"

#include "defs.h"
#include "rfswshared.h"

#include "timebase.h"
#include "myalloc.h"
#include "settings.h"
#include "dbgupdc.h"
#include "stream.h"
#include "buffer.h"

#include "matriximages.h"

#include <rf/rf.h>
#include <crc8/crc8.h>

#include <stdlib.h>

/* Buglist
 * 	9.8.2013: Do not enable the RS232 Receiver, until the Rx pin is connected to somewhere.
 */

#define VOLTAGE_FACTOR (0.7674*4.3) // factor to convert from Lithium-Voltage to lead acid voltage (nominal 12V)
#define VOLTAGE_POWER_OFF 10.5 // Voltage in Volts

// global volatile variables:
	static volatile bool receiveFlag = false; // this flag is set by the Pin-changing interrupt, it is cleared in main

// global variables:
	static SETTINGS* settings;
	static Pin pinButton = PIN_PUSHBUTTON_1;
	static Pin pinOffSwitch = PIN_OFF_SWITCH;
	static Pin pinRelais = PIN_RELAIS;
//	static TIME_T tOfLastBoatBoxAlive = TIME_INVALID;
	static bool gotNewPacket=false;
	static uint8_t fullBitmap[MATRIX_MODULES][MATRIX_ROWS];
	static bool transmitBitmap=false; // is set in the receive-handler
		// and cleared on transmit-finished
	static int16_t recentShownImageID=-1;



// global objects:
	static TRX_Object* trxB=NULL;
	static RF_Object* rf=NULL;
	static const Pin pPinsRS232[]={BOARD_RS232_PINS};
//	static MESSAGE* currentMessage = NULL; // current command-transmit-message to flipdots (one image)

// function declarations:
	static void RfB_ReceiveHandler(RF_Object* rfObj);
	static void ConfigureRS232();
	static void printImageToDBGU();

	static void walkingLinesStep();
	static void rainingPixelStep();
	static void fillImage(bool yellow);
	static void setRow(uint8_t row, bool yellow);
	static void setColumn(uint8_t column, bool yellow);
	static bool sendImageToFlipdots();
	static void powerOff();
	static void slowProcessMessageQueue();


// ISR for RS485; Rx-Data available
//static void ISR_RS232()
//{
//	if(pUsartRS232->US_CSR & 1) // if RxRDY
//	{
//		//Buf_push(rxBuf, (uint8_t)(pUsartRS232->US_RHR&0xff));
//	}
//}

// Any Packet was received, forward IRQ to software RF-Module
static void ISR_TrxB_GD0_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
	// DCL_Gd0ChangedHandler(trxB);

	TRACE_DEBUG("GDO0 changed\n");
	if (PIO_Get(&trxB->settings.pins.gd0)) // rising edge
	{
		// do not call the receive handler directly but in main when this flag is set.
		receiveFlag = true;
	}
	else // falling edge
	{

	}
}

static void ISR_Button_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
}

void RunMatrixBoard(SETTINGS* set)
{
	settings = set;
	printf("Running as Matrix-Board\n\n");
	SET_print(set);

	DCL_Command cmd;
	trxB = myalloc(sizeof(TRX_Object));
	rf = myalloc(sizeof(RF_Object));

	TRACE_DEBUG("Configure User-Button...");
	PIO_Configure(&pinButton, 1);
	PIO_ConfigureIt(&pinButton, ISR_Button_changed);
	PIO_EnableIt(&pinButton);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure OFF-Switch...");
	PIO_Configure(&pinOffSwitch, 1);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure Relais-Pin...");
	PIO_Configure(&pinRelais, 1);
	RELAIS_ON; // activate selfpowering!
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure TRX Module B (433MHz)...");
	TRX_constructor(trxB, TRX_PINSET_B, TRX_CONFIG_CC1101);
	TRX_init(trxB);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Init Console...");
	DCL_Init(trxB, NULL);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure GDO0-Interrupt...");
	PIO_ConfigureIt(&(trxB->settings.pins.gd0), ISR_TrxB_GD0_changed);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Setup RF-Communication...");
	RF_Setup(rf, NUM_OF_SLOTS_433M, TICS_PER_SLOT_433M, trxB, set->address_433M,
			RfB_ReceiveHandler, TRANSMIT_DELAY_100us_433M, TRANSMIT_DELAY_thirdMicroSec_433M);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Enable Interrupt for GDO0...");
	PIO_EnableIt(&(trxB->settings.pins.gd0));
	TRACE_DEBUG_WP("[OK]\n");


//	RF_LoadSync(rf); // first sync

	ConfigureRS232(); // for streaming to RS485 (flipdot modules)
	init_streams();

	enum RunState{Manager=0, PixelRain, WalkingLines, FlipFullScreen,
		MKZ_Presentation, MKZ_PresentationUpsideDown, Stop} runState = Manager;

	// clear flipdots:
	memcpy(fullBitmap, MatrixImageKibokoZeitnehmung, sizeof(fullBitmap));
	TIME_T now = time_100us;
	while(time_100us < now+10000); // wait 1s, so the Step-Up converter and the
		// flip-dot modules can power up, and then clear the image.
	sendImageToFlipdots();

	ALIVE_PACKET packet =
	{
		.batteryVoltageTS = 0xffff,
		.triggerTimeTS_L = TIME_INVALID,
		.triggerTimeTS_R = TIME_INVALID,
		.boatBoxID = 0xFF, // invalid boat box
		.batteryVoltageBB = 0xffff,
		.triggerTimeBB = TIME_INVALID, // send the last trigger time
		.binRssiBB = 0,
		.stationTriggeredAt=0
	};


	while(1)
	{
		KICK_DOG;
		switch(runState)
		{
			case Manager:
			{
				while(pitFlag == false);
				pitFlag = false;
				if(RF_Process(rf))
				{
	//				LED_Toggle(LED_DS_TxB);
					// Update battery voltages:
				}

				if(receiveFlag)
				{
					receiveFlag = false;
					if(RF_Receive(rf))
					{
						LED_Toggle(LED_DS_RxB);
					}
				}

				if(rf->synchronized)
				{
					LED_Set(LED_DS1);
				}else
				{
					LED_Clear(LED_DS1);
				}

				static uint8_t transmitCounter = true;
				static TIME_T recentTransmitTime = 0;
				if(transmitBitmap)
				{
					if(transmitCounter>0 && time_100us > recentTransmitTime+10000)
					{
						if(sendImageToFlipdots()) // on success
						{
							recentTransmitTime = time_100us;
							transmitCounter--;
						}
					}
					if(transmitCounter <= 0)
					{
						transmitCounter = 2; // transmit every image twice.
						transmitBitmap = false; // clear flag for receive-handler
					}
				}

				// Auto-Alive-Packet:
				static TIME_T recentTransmit=0;
				static const TIME_T TransmitDelay = 10000/2; // max. 8 packets per second for "economy" packets
				if(rf->synchronized && rf->fifoFilled == false &&
						time_100us > recentTransmit + TransmitDelay) // enough time elapsed for economy-packet
				{
					recentTransmit = time_100us;

					// prepare packet:
					packet.batteryVoltageTS = v1*VOLTAGE_FACTOR*1000; // convert voltage from the 10k/33k voltage-divider (Lithium Cell)
						// to the 33k/10k divider for the lead acid battery.
						// decimal fixed point: 1234 = 1.234V;
					packet.triggerTimeTS_L = TIME_INVALID;
					packet.triggerTimeTS_R = TIME_INVALID;
					packet.boatBoxID = 0xFF;
					packet.batteryVoltageBB = 0xFFFF;
					packet.triggerTimeBB = TIME_INVALID;
					packet.stationTriggeredAt = 0xFF;

					RF_LoadFifo(rf, RF_ADDR_MASTER_0, (uint8_t*)&packet, sizeof(ALIVE_PACKET));
				}
			}break;

			case PixelRain:
			{
				KICK_DOG;
				static TIME_T recentStep=0;

				if(time_100us >= recentStep+10000/9)
				{
					//printf("Demo Step\n");
					recentStep = time_100us;
					//walkingLinesStep();
					rainingPixelStep();
				}
				processMessageQueue(); // to here the extra processMessageQueue()
				 // so the flipdots are working at their max. framerate.
			}break;

			case WalkingLines:
			{
				KICK_DOG;
				static TIME_T recentStep=0;

				if(time_100us >= recentStep+10000/3)
				{
					//printf("Demo Step\n");
					recentStep = time_100us;
					walkingLinesStep();
				}
			}break;

			case FlipFullScreen:
			{
				KICK_DOG;
				static TIME_T recentStep=0;
				static bool yellow = true;

				if(time_100us >= recentStep+10000*2)
				{
					//printf("Demo Step\n");
					recentStep = time_100us;
					fillImage(yellow);
					sendImageToFlipdots();
					if(yellow) yellow = false;
					else yellow = true;
				}
			}break;

			case MKZ_Presentation:
			case MKZ_PresentationUpsideDown:
			{
				static int mkzState = 0;
				static TIME_T recentStep=0;

				switch(mkzState)
				{
				case 0:
					memcpy(fullBitmap, MatrixImageMKZ_1a_MKZ, sizeof(fullBitmap));
					sendImageToFlipdots();
					mkzState++;
					recentStep = time_100us;
					break;
				case 1:
					if(time_100us >= recentStep + 5*10000)
						mkzState++;
					break;
				case 2:
					memcpy(fullBitmap, MatrixImageMKZ_1b_Mechatronik_KarlZeilhofer, sizeof(fullBitmap));
					sendImageToFlipdots();
					mkzState++;
					recentStep = time_100us;
					break;
				case 3:
					if(time_100us >= recentStep + 10*10000)
						mkzState++;
					break;
				case 4:
					memcpy(fullBitmap, MatrixImageMKZ_1c_Steinbach_amZiehberg, sizeof(fullBitmap));
					sendImageToFlipdots();
					mkzState++;
					recentStep = time_100us;
					break;
				case 5:
					if(time_100us >= recentStep + 3*10000)
						mkzState++;
					break;
				case 6:
					memcpy(fullBitmap, MatrixImageMKZ_2_Elektronik_Entwicklung, sizeof(fullBitmap));
					sendImageToFlipdots();
					mkzState++;
					recentStep = time_100us;
					break;
				case 7:
					if(time_100us >= recentStep + 3*10000)
						mkzState++;
					break;
				case 8:
					memcpy(fullBitmap, MatrixImageMKZ_3_SystemPro__grammierung, sizeof(fullBitmap));
					sendImageToFlipdots();
					mkzState++;
					recentStep = time_100us;
					break;
				case 9:
					if(time_100us >= recentStep + 3*10000)
						mkzState++;
					break;
				case 10:
					memcpy(fullBitmap, MatrixImageMKZ_4_Prototypen_Fertigung, sizeof(fullBitmap));
					sendImageToFlipdots();
					mkzState++;
					recentStep = time_100us;
					break;
				case 11:
					if(time_100us >= recentStep + 3*10000)
						mkzState++;
					break;
				case 12:
					memcpy(fullBitmap, MatrixImageMKZ_5_www_zeilhofer_coat, sizeof(fullBitmap));
					sendImageToFlipdots();
					mkzState++;
					recentStep = time_100us;
					break;
				case 13:
					if(time_100us >= recentStep + 5*10000)
						mkzState=0;
					break;
				}
			}break;

			case Stop:
			{
				KICK_DOG;
			}
		}

		slowProcessMessageQueue();

		{// update voltages, and implement low-voltage-shut-down
			static TIME_T lastTimeProcessed=0;
			static const TIME_T TimeOutDuration = 600000; // 60s, when voltage is low for 60s, shut off
			static TIME_T lastTimeEnoughVoltage=0;
			if(time_100us > lastTimeProcessed + 10000) // Update Voltages once a second
			{
				lastTimeProcessed = time_100us;
				UpdateVoltages();
				if(v1*VOLTAGE_FACTOR > VOLTAGE_POWER_OFF)
				{
					lastTimeEnoughVoltage = time_100us;
				}
				else // too low voltage
				{
					if(time_100us > lastTimeEnoughVoltage + TimeOutDuration)// &&
							//time_100us < lastTimeEnoughVoltage + 2*TimeOutDuration)
					{
						memcpy(fullBitmap, MatrixImageAkkuLeer, sizeof(fullBitmap));
						powerOff();
					}
				}
			}
		}

		if(DBGUPDC_IsRxReady()) // if Rx has bytes to be read
		{
			cmd = DCL_FeedWithChar(DBGUPDC_GetChar(NULL));
			if(cmd == DCL_cStop)
			{
				runState = Stop;
			}else if( cmd == DCL_cStart)
			{
				runState = Manager;
			}else if(cmd == DCL_cPrintrf)
			{
				printf("time_100us: %u\n", (unsigned int)time_100us);
				RF_Print(rf);
			}else if(cmd == DCL_cVolt)
			{
				UpdateVoltages();
				PrintVoltages();
			}else if(cmd == DCL_cMatrixTestStart)
			{
				runState = WalkingLines;
			}else if(cmd == DCL_cMatrixTestStop)
			{
				runState = Stop;
			}else if(cmd == DCL_cMatrixRainStart)
			{
				runState = PixelRain;
			}else if(cmd == DCL_cMatrixRainStop)
			{
				runState = Stop;
			}
		}


		// manage runState-transition and switching OFF:
		static TIME_T timeOfRecentPush = 0;
		static TIME_T timeOfRecentRelease = 0;
		TIME_T pushedDuration = 0;
		static bool pushed = false;
		if(PIO_Get(&pinOffSwitch) == 0 && !pushed) // if off-switch pressed = LOW
		{
			timeOfRecentPush = time_100us;
			pushed=true;
		}else if(PIO_Get(&pinOffSwitch) != 0 && pushed) // if off-switch released
		{
			timeOfRecentRelease = time_100us;
			pushed = false;

			pushedDuration = timeOfRecentRelease-timeOfRecentPush;
			printf("pushedDuration = %u\n", (unsigned int)pushedDuration);

			if(100 < pushedDuration && pushedDuration < 5000) // then switch off.
			{
				if(runState == WalkingLines)
				{
					fillImage(true);
				}else if(runState == FlipFullScreen ||
						runState == MKZ_Presentation || runState == MKZ_PresentationUpsideDown)
				{
					// do nothing, leave current image
				}
				else
				{
					memcpy(fullBitmap, MatrixImageKibokoZeitnehmung, sizeof(fullBitmap));
					for(int m=0; m<MATRIX_MODULES; m++)
					{
						for(int r=0; r<MATRIX_ROWS; r++)
						{
							fullBitmap[m][r] |= MatrixImageOff[m][r]; // overlay the "off" in the upper right corner
						}
					}
				}
				powerOff();
			}
			else if(5000 <= pushedDuration) // then walk through all different modes.
			{
				switch(runState)
				{
				case Manager:
					runState = PixelRain;
					break;
				case PixelRain:
					runState = WalkingLines;
					break;
				case WalkingLines:
					runState = FlipFullScreen;
					break;
				case FlipFullScreen:
					runState = MKZ_Presentation;
					break;
				case MKZ_Presentation:
					runState = MKZ_PresentationUpsideDown;
					break;
				case MKZ_PresentationUpsideDown:
					runState = Manager;
					recentShownImageID = -1;
					break;

				case Stop:
					// nothing to do.
					break;
				}
				fillImage(false); // clear image
			}
		}
	} // end of endless loop
}

// Data Packet was Received for this node
// this handler will be called by the software RF-Module
// do something with the data
static void RfB_ReceiveHandler(RF_Object* rfObj)
{
	static uint8_t bitmap[MATRIX_MODULES][MATRIX_ROWS]; // memory for collecting the image data [module][pixel-index]
		// pixelindex for each module is linewise, and each byte has 7 pixels (=MATRIX_COLS_PER_MOD)
	static bool subImageReceived[MATRIX_MODULES];
	static int16_t currentImageID = -1;

	//printf("Data Packet reveived\n");
	TRACE_DEBUG("Data Packet Received, len = %d\n", rfObj->rxLength);

	// packet from Time-Base
	MATRIX_PACKET* p;
	p = (MATRIX_PACKET*)(rfObj->rxBuffer+3);

//	int rssiBin = rfObj->rxBuffer[3+sizeof(MATRIX_PACKET)];
//	int senderAddr = rfObj->rxBuffer[1];

	if(p->packetType == PACKET_TYPE_MATRIX)
	{
		if(p->imageID != currentImageID)
		{
			printf("new image started\n");
			currentImageID = p->imageID;
			for(int n=0; n<MATRIX_MODULES; n++)
			{
				subImageReceived[n]=false;
			}
		}

		memcpy(&(bitmap[p->blocknumber][0]), p->datablock, MATRIX_ROWS);
		subImageReceived[p->blocknumber]=true;
		//printf("received image block %d\n", p->blocknumber);

		bool imCompleted=true;
		for(int n=0; n<MATRIX_MODULES; n++)
		{
			if(subImageReceived[n]==false)
			{
				imCompleted = false;
				break;
			}
		}

		if(imCompleted && transmitBitmap==false && recentShownImageID != currentImageID)
		{
			//printf("image received completely\n");

			memcpy(fullBitmap, bitmap, MATRIX_MODULES*MATRIX_ROWS);
			transmitBitmap = true;
			recentShownImageID = currentImageID;
		}

		gotNewPacket = true;
	}
	// TODO: implement command-packet-type (e.g. for reset)
}





// used for Communication via RS485 with the flipdot modules
// use transmit only! // TODO TWOWAY
static void ConfigureRS232()
{
	// setup hardware:
	PIO_Configure(pPinsRS232, 2);
	USART_Configure(pUsartRS232,
		0<<0| // mode (0=normal)
		0<<4| // clock select (0=MCK)
		3<<6| // Character length (3=8bits)
		0<<8| // synch. mode (0=asynchronous)
		4<<9| // Parity type (4=no parity)
		0<<12| // num. of stop bits (0=1bit)
		0<<14| // channel mode (0=normal)
		0<<16| // bit order (0=LSB; 1=MSB first)
		0<<17| // 9-bit char len (0=char.length defines length)
		0<<18| // drive SCK output (0=no drive SCK)
		0<<19| // oversampling mode (0=16x; 1=8x)
		0<<20| // INACK inhibit non acknowledge (0=the NACK is generated)
		0<<21| // DSNACK disable successive NACK (0=not disable SNACK)
		2<<24| // MAX_ITERATION in ISO7816 T0 mode
		0<<28  // FILTER for IrDA (0=no filter)
		, 38400, BOARD_MCK); //TODO: increase speed to 57600 (and change dip-switches) (9600,19200,38400,57600)
	PMC_EnablePeripheral(BOARD_RS232_USART_ID); // enable clock USART RS232

//	// set up interrupt:
//	const uint32_t mode = 5; // set priority to 5
	// mode = 0; i.e. priority = 0 << 0 (lowest)
	// 				  source type = 0 << 5 (level sensitive), is recommended for USART
//	AIC_ConfigureIT(BOARD_RS232_USART_ID, mode, ISR_RS232);
//	AIC_EnableIT(BOARD_RS232_USART_ID);
//	pUsartRS232->US_IER = AT91C_US_RXRDY;//  AT91C_US_TXBUFE is polled in the main loop

	// enable receive and transmit
	USART_SetTransmitterEnabled(pUsartRS232, 1);
//	USART_SetReceiverEnabled(pUsartRS232, 1);
}

static void printImageToDBGU()
{
	printf("\n");
	for(int line=0; line < MATRIX_ROWS; line++)
	{
		for(int module=0; module<MATRIX_MODULES; module++)
		{
			uint8_t ch = fullBitmap[module][line]; // 7 pixels in on byte
			for(int pixel=0; pixel<7; pixel++)
			{
				if(ch & (1<<(6-pixel)))
				{
					printf("#");
				}
				else
				{
					printf(".");
				}
			}
		}
		printf("\n");
	}
}


// call this every second
// all black
// all yellow
// all black
// 1 yellow line walking from top to bottom
// all yellow
// 1 black line walking from top to bottom
// all black
// one yellow column walking from left to right
// all yellow
// one black column walking from left to right
static void walkingLinesStep()
{
	static uint8_t line=0;
//	printf("line (begin) = %d\n", line); // WARNING: the static variable gets reinitialized, when
		// this printf is missing!!
		// the same problem was described here:
		// http://stackoverflow.com/questions/10629650/static-variable-reinitialization-in-gcc-under-linux

	static uint8_t col=0;
//	printf("col (begin) = %d\n", col);

	enum State {BLACK1=0,
		YELLOW1=1,
		BLACK2=2,
		YELLOW_WALKING_LINE=3,
		YELLOW2=4,
		BLACK_WALKING_LINE=5,
		BLACK3=6,
		YELLOW_WALKING_COLUMN=7,
		YELLOW3=8,
		BLACK_WALKING_COLUMN=9,
		LAST_DUMMY_STATE
	};
	static int state = BLACK1;

	switch(state)
	{
		case BLACK1:
		{
			fillImage(false);
			state++;
		}break;
		case YELLOW1:
		{
			fillImage(true);
			state++;
		}break;
		case BLACK2:
		{
			fillImage(false);
			state++;
		}break;
		case YELLOW_WALKING_LINE:
		{
			//printf("line = %d\n", line);

			if(line==0)
			{
				setRow(0, true);
				line++;
			}else
			{
				setRow(line-1, false);
				setRow(line, true);
				line++;
			}

			if(line>=(uint8_t)(MATRIX_ROWS))
			{
				//printf("go to next state");
				line=0;
				state++;
			}
			//printf("line = %d\n", line);
		}break;
		case YELLOW2:
		{
			fillImage(true);
			state++;
		}break;
		case BLACK_WALKING_LINE:
		{
			if(line==0)
			{
				setRow(0, false);
				line++;
			}else
			{
				setRow(line-1, true);
				setRow(line++, false);
			}

			if(line>=MATRIX_ROWS)
			{
				line=0;
				state++;
			}
		}break;
		case BLACK3:
		{
			fillImage(false);
			state++;
		}break;
		case YELLOW_WALKING_COLUMN:
		{
			if(col==0)
			{
				setColumn(0, true);
				col++;
			}else
			{
				setColumn(col-1, false); // clear old column
				setColumn(col++, true); // set new column
			}

			if(col>=MATRIX_COLS_PER_MOD*MATRIX_MODULES)
			{
				col=0;
				state++;
			}
		}break;
		case YELLOW3:
		{
			fillImage(true);
			state++;
		}break;
		case BLACK_WALKING_COLUMN:
		{
			if(col==0)
			{
				setColumn(0, false);
				col++;
			}else
			{
				setColumn(col-1, true);
				setColumn(col++, false);
			}

			if(col>=MATRIX_COLS_PER_MOD*MATRIX_MODULES)
			{
				col=0;
				state++;
			}
		}break;
		case LAST_DUMMY_STATE:
		default:
		{
			state = 0; // restart sequence again
		}
	}// end switch(state)


	sendImageToFlipdots();
//	printf("line (end) = %d\n", line);
//	printf("col (end) = %d\n", col);
//	printf("state (end) = %d\n", state);

}

static void fillImage(bool yellow)
{
	if(yellow)
	{
		memset(fullBitmap, 0x7F, sizeof(fullBitmap)); // fill image yellow
	}
	else
	{
		memset(fullBitmap, 0x00, sizeof(fullBitmap)); // fill image black
	}
}

static void setRow(uint8_t row, bool yellow)
{
	if(row >= MATRIX_ROWS)
	{
		return;
	}

	uint8_t p=0; // pixel pattern
	if(yellow)
	{
		p=0xFF;
	}

	for(int m=0; m<MATRIX_MODULES; m++) // for all modules
	{
		fullBitmap[m][row] = p;
	}
}

static void setColumn(uint8_t column, bool yellow)
{
	if(column >= MATRIX_MODULES*MATRIX_COLS_PER_MOD)
	{
		return;
	}

	uint8_t p=0; // pixel pattern
	uint8_t m=0; // module containing the column
	m = column/MATRIX_COLS_PER_MOD;
	p = 1<<(6-(column%MATRIX_COLS_PER_MOD));

	for(int r=0; r<MATRIX_ROWS; r++) // for all rows
	{
		if(yellow)
		{
			fullBitmap[m][r] |= p; // set bit
		}else
		{
			fullBitmap[m][r] &= ~p; // clear bit
		}
	}
}

static void setPixel(uint8_t column, uint8_t row, bool yellow)
{
	if(column >= MATRIX_MODULES*MATRIX_COLS_PER_MOD)
	{
		return;
	}
	if(row >= MATRIX_ROWS)
	{
		return;
	}

	uint8_t p=0; // pixel pattern
	uint8_t m=0; // module containing the column
	m = column/MATRIX_COLS_PER_MOD;
	p = 1<<(6-(column%MATRIX_COLS_PER_MOD));

	if(yellow)
	{
		fullBitmap[m][row] |= p; // set bit
	}else
	{
		fullBitmap[m][row] &= ~p; // clear bit
	}
}

// prints fullBitmap on the flipdots.
// TODO: evt auf speicher schreiben + aktualisieren umrüsten.
// gab bei ersten versuchen jedoch wieder pixelfehler.
static bool sendImageToFlipdots()
{
	const int SBZ = MATRIX_ROWS+4;// sub-block-size; for each module we have 4 additional bytes
	// (header byte, command byte, address byte, footer byte)

	static const int order[]={0,11,1,10,2,9,3,8,4,7,5,6};
//	static const int order[]={7,0,1,2,3,4,5,6,8,9,10,11};


//	static uint8_t buf[MATRIX_MODULES*(MATRIX_ROWS+4) ]; // using heap memory here
	static uint8_t buf[(MATRIX_ROWS+4)]; // using heap memory here

	for(int n=0; n<MATRIX_MODULES; n++) // for each module
	{
		buf[0] = 0x80; // header byte
		buf[1] = 0x83; // command byte (0x83=refresh immediatly, 0x84= only load into memory)
		buf[2] = order[n]; // address byte

		uint8_t m = order[n];

		// flip addressed due to the hardware bug of the flipdot-boards
		if(settings->boardNumber == 1)
		{
			if(m == 0)
				m = 6;
			else if(m == 6)
				m = 0;
		}else if(settings->boardNumber == 2)
		{
			if(m == 0)
				m = 9;
			else if(m == 9)
				m = 0;
		}

		memcpy(&(buf[3]), &(fullBitmap[m][0]), MATRIX_ROWS);
		buf[SBZ-1] = 0x8F; // end byte

		if(enqueueMsgRs(buf, sizeof(buf),  false) == NULL) // copies the buffer into a message-object
		{
			TRACE_WARNING("Error on adding flipdot-commands to message queue");
			return false;
		}
	}

	//printImageToDBGU();

	// append: refresh all panels
//	buf[MATRIX_MODULES * (MATRIX_ROWS + 4) + 0] = 0x80;
//	buf[MATRIX_MODULES * (MATRIX_ROWS + 4) + 1] = 0x82;
//	buf[MATRIX_MODULES * (MATRIX_ROWS + 4) + 2] = 0x8F;

	return true;
}

// call this function with max. framerate
// todo: when starting with blackDrop, the column gets one additional pixel at the top.
static void rainingPixelStep()
{
	srand(time_100us);
	static const int W = MATRIX_MODULES*MATRIX_COLS_PER_MOD; // width = number of columns

	static int8_t heads[MATRIX_MODULES*MATRIX_COLS_PER_MOD];
	static int8_t tails[MATRIX_MODULES*MATRIX_COLS_PER_MOD];
	static int8_t maxLen[MATRIX_MODULES*MATRIX_COLS_PER_MOD];
	static int8_t state[MATRIX_MODULES*MATRIX_COLS_PER_MOD]={0};
	static int8_t blackDrops[MATRIX_MODULES*MATRIX_COLS_PER_MOD];
	enum State{black, grow, blackDrop, shrink};

	for(int n=0; n<2; n++)
	{
		int x = rand()%(MATRIX_MODULES*MATRIX_COLS_PER_MOD);
		int y = rand()%(MATRIX_ROWS) - MATRIX_ROWS/2; // start in the upper half
		int len = rand()%(MATRIX_ROWS*4/5) + MATRIX_ROWS/5;

		for(int c=0; c<W; c++)
		{
			if(state[c] == grow)
			{
				if(heads[c]-tails[c] >= maxLen[c])
				{
					state[c] = blackDrop; // switch state
				}
				else
				{
					// move yellow head down
					heads[c]++;
					setPixel(c, heads[c], true);
				}
			}
			else if(state[c] == blackDrop)
			{
				if(heads[c]-blackDrops[c] < 0)
				{
					state[c] = shrink; // switch state
				}
				else
				{
					// move black drop down
					if(blackDrops[c] > tails[c])
					{
						setPixel(c, blackDrops[c]-1, true);
					}
					setPixel(c, blackDrops[c], false);
					blackDrops[c]++;
				}
			}
			else if(state[c] == shrink)
			{
				if(heads[c]-tails[c] <0)
				{
					state[c] = black;
				}
				else
				{
					// move black tail down
					setPixel(c, tails[c], false);
					tails[c]++;
				}
			}else if(state[c] == black && x==c)
			{
				// start new rainfall
				heads[c] = y;
				tails[c] = y;
				blackDrops[c]=y;
				maxLen[c] = len;
				state[c] = grow;
			}
		}
	}

	static const int speed = 0;
	static int counter = speed;
	if(counter <= 0)
	{
		counter = speed;
		sendImageToFlipdots();
	}else
	{
		counter--;
	}
}

// transmits the current fullImage, and then the power is turned off
static void powerOff()
{
	sendImageToFlipdots();

	TIME_T tTemp = time_100us;
	while(time_100us < tTemp + 10000)
	{
		KICK_DOG;
		slowProcessMessageQueue();
	}

	printf("Turning Kiboko Matrix OFF\n");

	RELAIS_OFF;
	while(1) // endless loop, wait for death
	{
		KICK_DOG;
	}
}

static void slowProcessMessageQueue()
{
	// delay the transmit processing:
	static TIME_T lastTimeProcessed=0;
	if(time_100us > lastTimeProcessed + 10000/50) // with 75 transmitts per second, pixel errors occur
		// with 50 modules per second, sometimes it comes to pixel errors
	{
		lastTimeProcessed = time_100us;
		processMessageQueue();
	}
}
