/*
 * matrixbox.h
 *
 *  Created on: 09.08.2013
 *      Author: Karl Zeilhofer
 */

#ifndef MATRIXBOARD_H_
#define MATRIXBOARD_H_

#include "settings.h"

void RunMatrixBoard(SETTINGS* set);

#endif /* MATRIXBOX_H_ */
