
#include "buffer.h"
#include <string.h> // for memcpy




/*
 *
 * ^
 * | Direction of memory address increase
 * --
 *	  <-- unused space
 * --
 *
 * --
 * 	  <-- topFreeIdx
 * --
 * o  <-- stored data
 * --
 * o
 * --
 * o
 * == <-- close ring of memory
 * o
 * --
 * o  <-- botDataIdx
 * --
 *
 * --
 *
 * --
 *
 *
 *
 */

// init the buffer
// user must provide an uninitialized buf-object and
// a memory location with the space in bytes.
void Buf_init(Buffer* buf, uint8_t* memory, int16_t space)
{
	buf->data = memory;
	buf->space=space;
	buf->botDataIdx=0;
	buf->topFreeIdx=0;
	buf->size=0;
}

// push an element onto the top of the stack
void Buf_push(Buffer* buf, uint8_t element) {
	if(buf->size < buf->space) // auf freien platz überprüfen
	{
		buf->data[buf->topFreeIdx]=element;
		
		buf->topFreeIdx++;
		buf->size++;
		if(buf->topFreeIdx>=buf->space)	{//Prüfen, ob Ende erreicht
			buf->topFreeIdx=0;	//Auf Anfang zurückstellen
		}
	}
}

// stuff an element at the bottom into the stack
void Buf_stuff(Buffer* buf, uint8_t element) {
	if(buf->size < buf->space) // check for free space
	{
		int16_t idx = buf->botDataIdx;
		idx--;
		if(idx<0)
		{
			idx = buf->space-1;
		}

		buf->data[idx]=element;
		buf->botDataIdx = idx;

		buf->size++;
	}
}

// pull an element from the bottom of the stack
uint8_t Buf_pull(Buffer* buf)	{
	uint8_t ch=buf->data[buf->botDataIdx];
	if(buf->size>0)
	{
		buf->size--;
	}
	buf->botDataIdx++;
	if(buf->botDataIdx>=buf->space){//Prüfen, ob Ende erreicht
		buf->botDataIdx=0;	//Auf Anfang zurückstellen
	}
	return ch;
}

// read the bottom element, but don't pull it out
uint8_t Buf_peekBottom(Buffer* buf)	{
	uint8_t ch=buf->data[buf->botDataIdx];
	return ch;
}

// read the top element, but don't pop it off
uint8_t Buf_peekTop(Buffer* buf)
{
	int16_t idx = buf->topFreeIdx;

	// get index of top data element
	idx--;
	if(idx<0)
	{
		idx = buf->space-1;
	}
	uint8_t ch=buf->data[buf->topFreeIdx];
	return ch;
}

// delete lower numElements elements from stack.
void Buf_deleteLower(Buffer* buf, int16_t numElements)
{
	// number of elements to delete:
	int16_t s = buf->size<numElements?buf->size:numElements;

	buf->botDataIdx = (buf->botDataIdx+s)%buf->space;
	buf->size-=s;

}

// copies data from the buffer to a linear memory-destination
// in the upward order (dataDestination[0]=bot, dataDestination[end]=top)
int16_t Buf_copyUpwardTo(Buffer* buf, uint8_t* dataDestination, int16_t maxSize)
{
	// get minimum of the
	int16_t s = buf->size<maxSize?buf->size:maxSize;

	// linear copy
	if(buf->topFreeIdx - buf->botDataIdx >= s)
	{
		memcpy(dataDestination, buf->data+buf->botDataIdx, s);
		return s;
	}

	// copy in two pieces:
	int16_t s1 = buf->space - buf->botDataIdx;
	memcpy(dataDestination, buf->data+buf->botDataIdx, s1);
	int16_t s2 = s-s1;
	memcpy(dataDestination+s1, buf->data, s2);
	return s;
}

// TODO!
void Buf_copyDownwardTo(Buffer* buf, uint8_t* dataDestination)
{
}
