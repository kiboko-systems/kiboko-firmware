# this file is built from a copy of the procetc mam-1.0 from Karl Zeilhofer

# ----------------------------------------------------------------------------
#         ATMEL Microcontroller Software Support 
# ----------------------------------------------------------------------------
# Copyright (c) 2008, Atmel Corporation
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice,
# this list of conditions and the disclaimer below.
#
# Atmel's name may not be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
# DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

# 	Makefile for compiling basic-internalflash-project

#-------------------------------------------------------------------------------
#		User-modifiable options
#-------------------------------------------------------------------------------

# Chip & board used for compilation
# (can be overriden by adding CHIP=chip and BOARD=board to the command-line)
CHIP  = at91sam7s256
BOARD = at91sam7s-kiboko

# Trace level used for compilation
# (can be overriden by adding TRACE_LEVEL=#number to the command-line)
# TRACE_LEVEL_DEBUG      5
# TRACE_LEVEL_INFO       4
# TRACE_LEVEL_WARNING    3
# TRACE_LEVEL_ERROR      2
# TRACE_LEVEL_FATAL      1
# TRACE_LEVEL_NO_TRACE   0
TRACE_LEVEL = 0

# Optimization level, put in comment for debugging
OPTIMIZATION = -Os

# AT91 library directory using Version 1.5
AT91LIB = ./at91lib

# Output file basename
OUTPUT = kiboko-firmware-17.0.1

# Compile for all memories available on the board (this sets $(MEMORIES))
# include $(AT91LIB)/boards/$(BOARD)/board.mak
MEMORIES = flash

# Output directories
BIN = bin
OBJ = obj

#-------------------------------------------------------------------------------
#		Tools
#-------------------------------------------------------------------------------

# Tool suffix when cross-compiling
# for WinARM use this:
# CROSS_COMPILE = arm-elf-
# for Sourcery CodeBench use: 
CROSS_COMPILE = arm-none-eabi-

# Compilation tools
CC = $(CROSS_COMPILE)gcc
SIZE = $(CROSS_COMPILE)size
STRIP = $(CROSS_COMPILE)strip
OBJCOPY = $(CROSS_COMPILE)objcopy

# Directories where source files can be found
PERIPH = $(AT91LIB)/peripherals
BOARDS = $(AT91LIB)/boards
UTILITY = $(AT91LIB)/utility
MEM = $(AT91LIB)/memories
COMMON = ./common


# Flags
INCLUDES = -std=c99 
INCLUDES += -I$(BOARDS)/$(BOARD) -I$(PERIPH)
INCLUDES += -I$(AT91LIB)/components -I$(AT91LIB)
INCLUDES += -I$(MEM)
INCLUDES += -I$(COMMON)


CFLAGS = -Wall -mlong-calls -ffunction-sections
CFLAGS += -g $(OPTIMIZATION) $(INCLUDES) -D$(CHIP) -DTRACE_LEVEL=$(TRACE_LEVEL) 
CFLAGS += -DOUTPUT=\"$(OUTPUT)\"
ASFLAGS = -g $(OPTIMIZATION) $(INCLUDES) -D$(CHIP) -D__ASSEMBLY__
LDFLAGS = -g $(OPTIMIZATION) -nostartfiles -Wl,--gc-sections

#-------------------------------------------------------------------------------
#		Files
#-------------------------------------------------------------------------------



VPATH += $(MEM)/flash
VPATH += $(UTILITY)
VPATH += $(PERIPH)/dbgu $(PERIPH)/aic $(PERIPH)/pit $(PERIPH)/pio $(PERIPH)/tc
VPATH += $(PERIPH)/pmc $(PERIPH)/spi $(PERIPH)/rstc $(PERIPH)/adc  
VPATH += $(PERIPH)/usart $(PERIPH)/pwmc $(PERIPH)/efc
VPATH += $(BOARDS)/$(BOARD) $(BOARDS)/$(BOARD)/$(CHIP)
VPATH += $(COMMON)/trx $(COMMON)/rf $(COMMON)/crc8


# Objects built from C source files
C_OBJECTS = main.o
C_OBJECTS += led.o stdio.o
C_OBJECTS += dbgu.o aic.o pio.o pio_it.o pit.o pmc.o tc.o spi.o usart.o rstc.o pwmc.o adc.o
C_OBJECTS += board_memories.o board_lowlevel.o 
C_OBJECTS += efc.o flashd_efc.o math.o
C_OBJECTS += trx.o rf.o crc8.o photodetect.o
C_OBJECTS += myalloc.o settings.o timebase.o triggerstation.o boatbox.o matrixboard.o matriximages.o dcl.o 
C_OBJECTS += dbgupdc.o rfswshared.o filter.o stream.o buffer.o boatboxbuffer.o packet_functions.o
C_OBJECTS += softpll.o


# Objects built from Assembly source files
ASM_OBJECTS = board_cstartup.o

# Append OBJ and BIN directories to output filename
OUTPUT := $(BIN)/$(OUTPUT)

#-------------------------------------------------------------------------------
#		Rules
#-------------------------------------------------------------------------------
# generell: "links target":"rechts dependencies"
#			TAB	"darunter rule"
# $<...first depencency
# $@...name of target
# $$...will be replaced with one dollar
# order of targets doesn't matter
# 

# Target all (for make all): make sub-targets bin obj and memories
all: $(BIN) $(OBJ) $(MEMORIES)

# make dir for all targets BIN and OBJ --> ./bin and ./obj
$(BIN) $(OBJ):
	mkdir $@


#define RULES: to use following code with different memories
define RULES
# add prefix for object files "./obj/" and for asm-files:
C_OBJECTS_$(1) = $(addprefix $(OBJ)/$(1)_, $(C_OBJECTS))
ASM_OBJECTS_$(1) = $(addprefix $(OBJ)/$(1)_, $(ASM_OBJECTS))


# target:dependencies
$(1): $$(ASM_OBJECTS_$(1)) $$(C_OBJECTS_$(1))
# rules:
# gcc with linkerflags and linker-script output to elf-file
	$(CC) $(LDFLAGS) -T"$(AT91LIB)/boards/$(BOARD)/$(CHIP)/$$@.lds" -o $(OUTPUT)-$$@.elf $$^
	$(OBJCOPY) -O binary $(OUTPUT)-$$@.elf $(OUTPUT)-$$@.bin
	$(SIZE) $$^ $(OUTPUT)-$$@.elf

$$(C_OBJECTS_$(1)): $(OBJ)/$(1)_%.o: %.c Makefile $(OBJ) $(BIN)
	$(CC) $(CFLAGS) -D$(1) -c -o $$@ $$< 

$$(ASM_OBJECTS_$(1)): $(OBJ)/$(1)_%.o: %.S Makefile $(OBJ) $(BIN)
	$(CC) $(ASFLAGS) -D$(1) -c -o $$@ $$<

debug_$(1): $(1)
	perl ../resources/gdb/debug.pl $(OUTPUT)-$(1).elf

endef

$(foreach MEMORY, $(MEMORIES), $(eval $(call RULES,$(MEMORY))))

clean:
	-rm -f $(OBJ)/*.o $(BIN)/*.bin $(BIN)/*.elf

