/*
 * Karl Zeilhofer
 * 28.3.2013
 *
 * this c-module implements a universal buffer
 *
 */


#ifndef BUFFER_H_
#define BUFFER_H_

#include <stdint.h>

//Buffer
typedef struct  {
	uint8_t* data;
	int16_t space;
	int16_t botDataIdx;
	int16_t topFreeIdx; // index der nächsten freien stelle
	int16_t size; // anzahl der elemente im puffer
}Buffer;

void Buf_init(Buffer* buf, uint8_t* memory, int16_t size);
void Buf_push(Buffer* buf, uint8_t element);
void Buf_stuff(Buffer* buf, uint8_t element);
uint8_t Buf_pull(Buffer* buf);
uint8_t Buf_peekBottom(Buffer* buf);
uint8_t Buf_peekTop(Buffer* buf);
// delete lower numElements elements from stack.
void Buf_deleteLower(Buffer* buf, int16_t numElements);
int16_t Buf_copyUpwardTo(Buffer* buf, uint8_t* dataDestination, int16_t maxSize);
#endif /* BUFFER_H_ */
