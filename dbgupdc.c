/*
 * dbgupdc.c
 *
 *  Created on: 13.07.2012
 *      Author: Karl Zeilhofer
 */

/*
 * This is a module for the Debug Unit (DBGU)
 * It handles Received Data via the Peripheral DMA Controller (PDC)
 */

#include "dbgupdc.h"
#include <board.h>
#include <stdlib.h> // for NULL

#define PDC_BUF_LEN		(1024)
	// TODO: bug beseitigen. großer Puffer ist nur workaround, weil switch zwichen 1 und 2 nicht funktoiniert.
typedef struct
{
	uint8_t ab; // index of buffer, which is currently read (0 or 1)
	uint8_t i; // number of read bytes of one buffer (0...PDC_BUF_LEN)
	const uint16_t len; // length of each buffer, just an abbreviation for PDC_BUF_LEN
	volatile uint8_t buf[2][PDC_BUF_LEN]; // this buffer is filled by the DMA controller.
}DBGU_RxBuffer;

static DBGU_RxBuffer self = {.ab=0, .i=0, .len=PDC_BUF_LEN, .buf={{0},{0}}};


void DBGUPDC_Init()
{
	AT91C_BASE_DBGU->DBGU_RPR = (AT91_REG)&(self.buf[0][0]);  // Receive Pointer Register
	AT91C_BASE_DBGU->DBGU_RCR = self.len;  // Receive Counter Register
	AT91C_BASE_DBGU->DBGU_RNPR = (AT91_REG)&(self.buf[1][0]); // Receive Next Pointer Register
	AT91C_BASE_DBGU->DBGU_RNCR = self.len; // Receive Next Counter Register
	AT91C_BASE_DBGU->DBGU_PTCR = AT91C_PDC_RXTEN; // enable receive transfer
}

bool DBGUPDC_IsRxReady()
{
	uint16_t counterReg;

	if(!(AT91C_BASE_DBGU->DBGU_PTSR & (1<<3)) ) // if not End of Receive Transfer (=primary Rx Buffer is full)
	{
		counterReg = (uint16_t)(AT91C_BASE_DBGU->DBGU_RCR);
	}else
	{
		// because the primary buffer is full, the counter register belonging to this buffer is zero.
		counterReg = 0;
	}

	if(self.i + counterReg < self.len)
	{
		return true;
	}else
	{
		return false;
	}
}

uint8_t DBGUPDC_GetChar(bool* ok)
{
	uint8_t c=0;
	uint16_t counterReg;

	if(!(AT91C_BASE_DBGU->DBGU_PTSR & (1<<3)) ) // if not End of Receive Transfer (=primary Rx Buffer is full)
	{
		counterReg = (uint16_t)(AT91C_BASE_DBGU->DBGU_RCR);
	}else
	{
		// because the primary buffer is full, the counter register belonging to this buffer is zero.
		counterReg = 0;
	}

	if(self.i + counterReg < self.len) // if char available
	{
		// read one and return it
		c = self.buf[self.ab][self.i];
		self.i++; // increment "bytes read" counter
		if(ok!=NULL)
		{
			*ok = true;
		}
	}else
	{
		if(ok!=NULL)
		{
			*ok = false;
		}
	}

	if(self.i >= self.len) // if one buffer completly read
	{
		self.i = 0; // reset counter
		self.ab = (self.ab + 1)%2; // switch to the next buffer

		// prepare the next buffer
		AT91C_BASE_DBGU->DBGU_RNPR = (AT91_REG) &(self.buf[(self.ab+1)%2][0]); // Receive Next Pointer Register
		AT91C_BASE_DBGU->DBGU_RNCR = self.len; // Receive Next Counter Register
	}

	return c;
}
