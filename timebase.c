/*
 * timebase.c
 *
 *  Created on: 06.07.2012
 *      Author: karl
 */

#include "defs.h"
#include "rfswshared.h"

#include "timebase.h"
#include "myalloc.h"
#include "settings.h"
#include "dbgupdc.h"
#include "stream.h"
#include "buffer.h"

#include <rf/rf.h>
#include <crc8/crc8.h>


//#define INFO		// build the INFO struct and print it on the serial port


// global volatile variables:
	static volatile bool receiveFlag = false; // this flag is set by the Pin-changing interrupt, it is cleared in main

// global variables:
	static Pin pinButton = PIN_PUSHBUTTON_1;
//	static TIME_T tOfLastBoatBoxAlive = TIME_INVALID;
	static bool gotNewPacket=false;
	static TIME_T tOfLastSend2PC = 0;
	static bool timeSetFlag=false; // this flag is set if the first PC-time is received!

	// Command Bytes (Commands can be sent from the Kiboko-Manager to the RF-Devices)
	// TODO 3: ensure packet data integrety!!!, values are changed in an ISR!



// global objects:
	static TRX_Object* trxB=NULL;
	static RF_Object* rf=NULL;
	static const Pin pPinsRS232[]={BOARD_RS232_PINS};
	static const Pin pPinsRS232_ALGE[]={BOARD_RS232_ALGE_PINS};
	static Buffer _buffer;
	static Buffer* rxBuf = &_buffer;

	extern AT91PS_USART pUsartRS232_ALGE;

	typedef struct sCommandQueueItem
	{
		uint16_t cmdReceiver;
		uint8_t command;
		uint16_t commandData;
		uint16_t sentCountdown; // set to a number for new items, decreased when item was sent
	}CommandQueueItem;

#define COMMAND_QUEUE_SIZE 10
	typedef struct sCommandQueue
	{
		int size;
		CommandQueueItem items[COMMAND_QUEUE_SIZE];

		int iNextOut; // take the next element from this index, and increment the index then.
		int iNextIn; // store a new element to this index, and increment the index then.
		// if iNextOut == iNextIn --> Queue is empty
	}CommandQueue;

	CommandQueue cmdQ;


// function declarations:
	static void RfB_ReceiveHandler(RF_Object* rfObj);
	static void initInfo();
	static void ConfigureRS232();
	static void ConfigureRS232_ALGE();
#ifdef INFO
	static void printInfoScreen();

// information structure for collecting the data from all boxes
	#define N_LAPS 3
	typedef struct
	{
		TIME_T time;
		int voltage; // in mV
	}TIME_BASE;

	typedef struct
	{
		int rssi; // in dBm
		int voltage; // in mV
		TIME_T tOfLastAlive; // save current time of Time-Base, when a packet coming
			// from the Trigger-Station was received.
		TIME_T triggerTimeL; // time of last trigger on the left side
		TIME_T triggerTimeR; // time of last trigger on the right side
	}TRIGGER_STATION;

	typedef struct
	{
		int rssi[2]; // in dBm; forwarded by TS1/TS2
		int voltage; // in mV
		TIME_T tOfLastAlive; // save current time of Time-Base, when a packet coming
			// from the Boat-Box was received.

		// xxxx[0]..oldest
		// xxxx[N_LAPS-1]...newest
		TIME_T startTimes[N_LAPS];
		TIME_T finishTimes[N_LAPS]; //
		int laps[N_LAPS]; // lap number
		int currentLap;
		bool isRunning;
	}BOAT_BOX;

	typedef struct
	{
		TIME_BASE timeBase;
		TRIGGER_STATION triggerStations[2];
		BOAT_BOX boatBoxes[2];
	}INFO;

	static INFO info;
#endif

// ISR for RS232 (XPORT) Rx-Data available
static void ISR_RS232()
{
	if(pUsartRS232->US_CSR & 1) // if RxRDY
	{
		if(rxBuf->size < rxBuf->space)
		{
			Buf_push(rxBuf, (uint8_t)(pUsartRS232->US_RHR&0xff));
		}else
		{
			TRACE_WARNING("XPORT Rx-Buffer full (%s line %d)", __FILE__, __LINE__);
		}
	}
}

// Any Packet was received, forward IRQ to software RF-Module
static void ISR_TrxB_GD0_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
	// DCL_Gd0ChangedHandler(trxB);

	TRACE_DEBUG("GDO0 changed\n");
	if (PIO_Get(&trxB->settings.pins.gd0)) // rising edge
	{
		// do not call the receive handler directly but in main when this flag is set.
		receiveFlag = true;
	}
	else // falling edge
	{

	}
}

static void ISR_Button_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
}

void RunTimeBase(SETTINGS* set)
{
	printf("Running as Time-Base\n\n");
	SET_print(set);

	DCL_Command cmd;
	trxB = myalloc(sizeof(TRX_Object));
	rf = myalloc(sizeof(RF_Object));

	TRACE_DEBUG("Configure User-Button...")
	PIO_Configure(&pinButton, 1);
	PIO_ConfigureIt(&pinButton, ISR_Button_changed);
	PIO_EnableIt(&pinButton);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure TRX Module B (433MHz)...");
	TRX_constructor(trxB, TRX_PINSET_B, TRX_CONFIG_CC1101);
	TRX_init(trxB);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Init Console...");
	DCL_Init(trxB, NULL);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure GDO0-Interrupt...");
	PIO_ConfigureIt(&(trxB->settings.pins.gd0), ISR_TrxB_GD0_changed);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Setup RF-Communication...");
	RF_Setup(rf, NUM_OF_SLOTS_433M, TICS_PER_SLOT_433M, trxB, set->address_433M,
			RfB_ReceiveHandler, TRANSMIT_DELAY_100us_433M, TRANSMIT_DELAY_thirdMicroSec_433M);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Enable Interrupt for GDO0...");
	PIO_EnableIt(&(trxB->settings.pins.gd0));
	TRACE_DEBUG_WP("[OK]\n");

	RF_LoadSync(rf); // first sync

	initInfo();

	static uint8_t bufData[1024];
	Buf_init(rxBuf, bufData, 1024);
	ConfigureRS232(); // for streaming
	ConfigureRS232_ALGE(); // for ALGE-displays
	init_streams();

	// data for matrix displays
	uint8_t matrix_data1[MATRIX_MODULES*MATRIX_ROWS]={0};
	uint8_t matrix_data2[MATRIX_MODULES*MATRIX_ROWS]={0};



	uint8_t matrixImageID_1=0;
	uint8_t matrixImageID_2=0;

	/* RF-cycle:
	 * 0 sync
	 * 1 MATRIX 1
	 * 2 sync/radio-command
	 * 3 MATRIX 2
	 * ...
	 */
	int rfCycle=0;
	int matrixBlockCounter_1=0;
	int matrixBlockCounter_2=0;

	// Init Command Queue:
	cmdQ.size = COMMAND_QUEUE_SIZE;
	cmdQ.iNextIn=0;
	cmdQ.iNextOut=0;

	bool isRunning = true;
#ifdef INFO
	TIME_T tOfLastPrint = 0;
#endif

	while(1)
	{
		KICK_DOG;
		if(isRunning)
		{
			while(pitFlag == false);
			pitFlag = false;
			if(RF_Process(rf))
			{
//				LED_Toggle(LED_DS_TxB);
				// Update battery voltages:
				UpdateVoltages();
			}

			// if packet was sent, load next packet:

			// we have 4 differnt packet types, which will be sent cyclic.
			// with 6 nodes in the 4330MHz network (incl. masters), a slot time of 2ms
			// one complete cycle needs 6 nodes * 2ms/node * 4 cycle steps = 48ms
			// 0. sync packet
			// 1. data for matrix 1
			// 2. optional command packet, otherwise sync packet again
			// 3. data for matrix 2
			if(rf->fifoFilled == false)
			{
				switch(rfCycle)
				{
					case 0:
					{
						RF_LoadSync(rf);
						rfCycle++;
						break;
					}
					case 1:
					{
						// send datablock for MATRIX 1
						MATRIX_PACKET packet;
						packet.packetType=PACKET_TYPE_MATRIX;
						packet.imageID=matrixImageID_1;
						packet.blocknumber=matrixBlockCounter_1;
						memcpy(packet.datablock, &matrix_data1[matrixBlockCounter_1*MATRIX_BLOCK_SIZE], MATRIX_BLOCK_SIZE);
						RF_LoadFifo(rf, 4, (uint8_t*)&packet, sizeof(packet));

						matrixBlockCounter_1++;
						if(matrixBlockCounter_1>=MATRIX_N_DATABLOCKS)
						{
							matrixBlockCounter_1=0;
						}
						rfCycle++;
						break;
					}
					case 2:
					{
						if((cmdQ.iNextOut != cmdQ.iNextIn)) // when we have unsent commands in the queue
							// TODO 3: paket-bremse implementieren, priorität sollte das sync-paket haben!
						{
							printf("a command has to be sent\n");
							COMMAND_PACKET packet;
							packet.packetType = PACKET_TYPE_COMMAND;

							packet.cmdReceiver = cmdQ.items[cmdQ.iNextOut].cmdReceiver;
							packet.command = cmdQ.items[cmdQ.iNextOut].command;
							packet.commandData = cmdQ.items[cmdQ.iNextOut].commandData;

							uint8_t destAddr; // rf address for the 433MHz Network
							if((packet.cmdReceiver & 0xFF00) == 0) // if for the 433MHz-Devices
							{
								destAddr = packet.cmdReceiver & 0x00FF; // use the direct address
								printf("destAddress is in 433MHz Network\n");
							}else // it is for the 2.4GHz devices
							{
								destAddr = 2; // to Trigger-Station Start (used as a bridge)
								printf("destAddress is in 2.4GHz Network\n");
								// TODO 3: send this packet also to TSG!
								// at least manage how the packet is forwarded.
							}
							if(RF_LoadFifo(rf, destAddr, (uint8_t*)&packet, sizeof(packet)) != 0) // if no error
							{
								TRACE_WARNING("Error on loading Command-Packet into the FIFO for transmit\n");
							}else
							{
								printf("Sent a radio command via RF\n");
								cmdQ.items[cmdQ.iNextOut].sentCountdown--;
								if(cmdQ.items[cmdQ.iNextOut].sentCountdown == 0)
								{
									cmdQ.iNextOut = (cmdQ.iNextOut+1)%cmdQ.size;
								}
							}
						}else{ // if there is no command to be forwarded...
							RF_LoadSync(rf);
						}

						rfCycle++;

						break;
					}
					case 3:
					{
						// send datablock for MATRIX 2
						MATRIX_PACKET packet;
						packet.packetType=PACKET_TYPE_MATRIX;
						packet.imageID=matrixImageID_2;
						packet.blocknumber=matrixBlockCounter_2;
						memcpy(packet.datablock, &matrix_data2[matrixBlockCounter_2*MATRIX_BLOCK_SIZE], MATRIX_BLOCK_SIZE);
						RF_LoadFifo(rf, 5, (uint8_t*)&packet, sizeof(packet));

						matrixBlockCounter_2++;
						if(matrixBlockCounter_2>=MATRIX_N_DATABLOCKS)
						{
							matrixBlockCounter_2=0;
						}
						rfCycle=0;
						break;
					}
				}
			}

			if(receiveFlag)
			{
				receiveFlag = false;
				if(RF_Receive(rf))
				{
					LED_Toggle(LED_DS_RxB);
				}
			}

#ifdef INFO
			if(time_100us > tOfLastPrint+4999) // print every 0.5s
			{
				gotNewPacket = false;
				tOfLastPrint = time_100us;
				printInfoScreen();
			}
#endif

			// auto packet to PC
			if(time_100us > tOfLastSend2PC + 49999)
			{
				//printf("send autopacket to PC\n");
				tOfLastSend2PC=time_100us;
				TB2PC_PACKET tb2pc;

				// make auto-packet to PC
				tb2pc.magicNumber1=MAGIC_NUMBER1;
				tb2pc.magicNumber2=MAGIC_NUMBER2;
				tb2pc.packetVersion=PACKET_VERSION;
				if(timeSetFlag)
				{
					tb2pc.baseTime_100us = time_100us;
				}
				else
				{
					tb2pc.baseTime_100us = TIME_INVALID;
				}

				ALIVE_PACKET p;							// dummy packet
				p.boatBoxID=BOX_PACKET_ID_INVALID;		// invalid BBID
				tb2pc.tsPacket=p;						// copy the alive-packet

				tb2pc.batteryVoltageTB=vMin*1000;				// autocast from float to int
				tb2pc.triggerStationID=BOX_PACKET_ID_INVALID;	// invalid TSID

				tb2pc.crc=CRC8_run((uint8_t*)&tb2pc, sizeof(tb2pc)-1);	// calculate the crc (but not from crc itself)

				// send packet to PC
				if(enqueueMsgRs(&tb2pc, sizeof(tb2pc), 0)==NULL)
				{
					TRACE_WARNING("Could not enqueue message to PC (Line %d)\n", __LINE__);
				}
				else
				{
					//printf("auto packet to PC sent\n");
				}
			}

			// it's the time-base, we are always synchronized!
			// this is needed, to keep sync-packets running, when the clock is set
			// manually to the future!
			rf->synchronized = true;
			rf->lastSyncTime = time_100us;
		}


		// receive PC2TB packet
		if(rxBuf->size>=sizeof(PC2TB_PACKET))					// enough data available, start reading packet
		{
			static PC2TB_PACKET inPacket; // use static, so it can be used for the DMA

			if(Buf_peekBottom(rxBuf) == MAGIC_NUMBER1)			// check first magic number
			{
				inPacket.magicNumber1=Buf_pull(rxBuf);

				if(Buf_peekBottom(rxBuf) == MAGIC_NUMBER2)
				{
					inPacket.magicNumber2=Buf_pull(rxBuf);		// check second magic number

					// magic numbers checked, try to read data
					uint8_t* data = (uint8_t*)(&inPacket);
					Buf_copyUpwardTo(rxBuf, data+2, (int16_t)(sizeof(inPacket)-2));
					Buf_deleteLower(rxBuf, sizeof(inPacket)-2);

					// check CRC and packet version
					if(CRC8_run((uint8_t*)(&inPacket), sizeof(inPacket))==0 && inPacket.packetVersion==PACKET_VERSION)
					{
						// use the packet data:
						if(!timeSetFlag)
						{
							// set Timebase to PC time
							timeSetFlag=true;
							LED_Set(LED_DS1);
							time_100us = inPacket.pcTime_100us;
						}


						// copy MATRIX display data

						if(inPacket.matrixDataValid)
						{
							memcpy(matrix_data1, inPacket.matrixData1, MATRIX_DATA_SIZE);
							matrixImageID_1++;			// this runs over
							matrixBlockCounter_1=0;		// abort current image (finished or not) and start next one


							memcpy(matrix_data2, inPacket.matrixData2, MATRIX_DATA_SIZE);
							matrixImageID_2++;			// this runs over
							matrixBlockCounter_2=0;		// abort current image (finished or not) and start next one
						}

						if(inPacket.cmdReceiver != COMMAND_INVALID_RECEIVER &&
								inPacket.command != RCmd_INVALID)
						{
							printf("got a radio command from PC\n");

							if(((cmdQ.iNextIn+1)%cmdQ.size) != cmdQ.iNextOut) // if we still have free space
							{
								cmdQ.items[cmdQ.iNextIn].cmdReceiver	= inPacket.cmdReceiver;
								cmdQ.items[cmdQ.iNextIn].command		= inPacket.command;
								cmdQ.items[cmdQ.iNextIn].commandData	= inPacket.commandData;
								cmdQ.items[cmdQ.iNextIn].sentCountdown	= 5;

								cmdQ.iNextIn = (cmdQ.iNextIn+1)%cmdQ.size;
							}else
							{
								printf("Command Queue is full\n");
							}

							printf("  %d,%d,%d\n",inPacket.cmdReceiver, inPacket.command, inPacket.commandData);
						}

						// send data to ALGE display
						// this isn't done very frequently so we only use DMA and not streaming
						pUsartRS232_ALGE->US_TPR = (uint32_t) inPacket.algeText;
						pUsartRS232_ALGE->US_TCR = ALGE_TEXT_LEN;
						pUsartRS232_ALGE->US_PTCR = AT91C_PDC_TXTEN;
					}
					else
					{
						TRACE_WARNING("invalid packet data\n");
					}
				}
				// else: magicnumber 2 not found, try again next time with magicnumber 1
			}
			else
			{
				TRACE_WARNING("Magic Number not received, try to find it again\n");

				while(Buf_peekBottom(rxBuf)!=MAGIC_NUMBER1 && rxBuf->size>=1)
				{
					Buf_pull(rxBuf);		// remove incoming bytes as long as it is not a magic number
				}

			}
		}


		processMessageQueue();

		if(DBGUPDC_IsRxReady()) // if Rx has bytes to be read
		{
			cmd = DCL_FeedWithChar(DBGUPDC_GetChar(NULL));
			if(cmd == DCL_cStop)
			{
				isRunning = false;
			}else if( cmd == DCL_cStart)
			{
				isRunning = true;
			}else if(cmd == DCL_cPrintrf)
			{
				printf("time_100us: %u\n", (unsigned int)time_100us);
				RF_Print(rf);
			}else if(cmd == DCL_cVolt)
			{
				UpdateVoltages();
				PrintVoltages();
			}if(cmd == DCL_cSettime)
			{
#ifdef INFO
				tOfLastPrint = time_100us;
#endif
			}
		}
	} // end of endless loop
}

// Data Packet was Received for this node
// this handler will be called by the software RF-Module
// do something with the data
static void RfB_ReceiveHandler(RF_Object* rfObj)
{
	//printf("Data Packet reveived\n");
	TRACE_DEBUG("Data Packet Received, len = %d\n", rfObj->rxLength);

	// packet from TS
	ALIVE_PACKET* p;
	p = (ALIVE_PACKET*)(rfObj->rxBuffer+3);

	int rssiBin = rfObj->rxBuffer[3+sizeof(ALIVE_PACKET)];
	int senderAddr = rfObj->rxBuffer[1];
	int TSID = senderAddr-1; // Trigger-Station ID: is 1 or 2, Address is 2 or 3
#ifdef INFO
	int BBID = p->boatBoxID;
#endif

	if(timeSetFlag)		// only send packet if timebase is synchronized with PC
	{
		if(TSID>=1 && TSID<=4)			// check if TSID is valid (IDs 1-4 are used)
		{
			// packet to PC
			TB2PC_PACKET tb2pc;

			// make packet to pc
			tb2pc.magicNumber1=MAGIC_NUMBER1;
			tb2pc.magicNumber2=MAGIC_NUMBER2;
			tb2pc.packetVersion=PACKET_VERSION;
			tb2pc.tsPacket=*p;					// copy the alive-packet
			tb2pc.batteryVoltageTB=vMin*1000;	// autocast from float to int
			tb2pc.triggerStationID=TSID;		// TS where the packet comes from
			tb2pc.binRssiTS=rssiBin;			// RSSI of the connection from the TS to TB
			tb2pc.baseTime_100us = time_100us;  // send current Time-Base time to PC

			tb2pc.crc=CRC8_run((uint8_t*)&tb2pc, sizeof(tb2pc)-1);	// calculate the crc (but not from crc itself)

			// send packet to PC
			if(enqueueMsgRs(&tb2pc, sizeof(tb2pc), 0)==NULL)
			{
				TRACE_WARNING("Could not enqueue message to PC (Line %d)\n", __LINE__);
			}
			else
			{
				//printf("packet to PC sent\n");
			}

			// update last send time
			tOfLastSend2PC=time_100us;
		}
		else		// else invalid Trigger-Station-ID
		{
			TRACE_WARNING("Got Packet from TS%d with invalid Trigger-Station-ID (Line %d)\n", TSID, __LINE__);
		}
	}


#ifdef INFO

	if(TSID == 1 || TSID == 2) // Trigger-Station 1
	{
		info.triggerStations[TSID-1].rssi = TRX_num2rssi(rssiBin)/10; // in dBm
		info.triggerStations[TSID-1].tOfLastAlive = time_100us;
		info.triggerStations[TSID-1].triggerTimeL = p->triggerTimeTS_L;
		info.triggerStations[TSID-1].triggerTimeR = p->triggerTimeTS_R;
		info.triggerStations[TSID-1].voltage = p->batteryVoltageTS; // in mV

		if(BBID == 1 || BBID == 2)
		{
			info.boatBoxes[BBID-1].rssi[TSID-1] = TRX_num2rssi(p->binRssiBB)/10; // in dBm
		}
		else if(BBID == 0xff)
		{
			// just alive-packet from a Trigger-Station, without forwarding a
			// packet from a Boat-Box
		}else // invalid boatBoxID
		{
			TRACE_WARNING("Got Packet from TS%d with invalid Boat-Box-ID (Line %d)\n", TSID, __LINE__);
		}
	}
	else// else invalid Trigger-Station-ID
	{
		TRACE_WARNING("Got Packet from TS%d with invalid Trigger-Station-ID (Line %d)\n", TSID, __LINE__);
	}

	if(BBID == 1 || BBID == 2)
	{
		// handled above: info.boatBox1.rssi1
		// handled above: info.boatBox1.rssi2
		info.boatBoxes[BBID-1].tOfLastAlive = time_100us;
		info.boatBoxes[BBID-1].voltage = p->batteryVoltageBB; // in mV

		if(p->stationTriggeredAt == 1) // start
		{
			if(p->triggerTimeBB != info.boatBoxes[BBID-1].startTimes[N_LAPS-1]) // new lap!
			{
				info.boatBoxes[BBID-1].currentLap++;
				if(info.boatBoxes[BBID-1].isRunning) // boat hasnt finished started lap
				{ // then finish time = start time
					info.boatBoxes[BBID-1].finishTimes[N_LAPS-1] = info.boatBoxes[BBID-1].startTimes[N_LAPS-1];
				}
				// shift data in the arrays:
				for(int i = 0; i<N_LAPS-1; i++)
				{
					info.boatBoxes[BBID-1].startTimes[i] = info.boatBoxes[BBID-1].startTimes[i+1];
					info.boatBoxes[BBID-1].finishTimes[i] = info.boatBoxes[BBID-1].finishTimes[i+1];
					info.boatBoxes[BBID-1].laps[i] = info.boatBoxes[BBID-1].laps[i+1];
				}
				// set start time:
				info.boatBoxes[BBID-1].startTimes[N_LAPS-1] = p->triggerTimeBB;
				// set lap:
				info.boatBoxes[BBID-1].laps[N_LAPS-1] = info.boatBoxes[BBID-1].currentLap;
				// set running:
				info.boatBoxes[BBID-1].isRunning = true;
			}
		}
		else if(p->stationTriggeredAt == 2) // finish
		{
			if(info.boatBoxes[BBID-1].isRunning)
			{
				// set finish time:
				info.boatBoxes[BBID-1].finishTimes[N_LAPS-1] = p->triggerTimeBB;
				// clear running:
				info.boatBoxes[BBID-1].isRunning = false;
			}
			else
			{
				TRACE_INFO("Got packet from finish (TS2) of a non-running Boat-Box (line %d)\n", __LINE__);
			}

		}
		else if(p->stationTriggeredAt == 0xff)
		{
			// just alive-packet from a BoatBox, without a valid Trigger-Time
		}else // invalid StationTriggeredAt
		{
			TRACE_WARNING("Got Packet with invalid Station-Triggered-At (Line %d)\n", __LINE__);
		}

	}
	else if(BBID == 0xff)
	{
		// just alive-packet from a Trigger-Station, without forwarding a
		// packet from a Boat-Box
	}else // invalid boatBoxID
	{
		TRACE_WARNING("Got Packet with invalid Boat-Box-ID (Line %d)\n", __LINE__);
	}

#endif

	gotNewPacket = true;
}



static void initInfo()
{
	// TODO: init info-structure!
//	memset(&info, 0, sizeof(info)); // clear memory in info-structure
}

/*
 * Zeitbasis:
 *     Basiszeit: 12:34:56.7890    Akku: 75%
 * Start:
 * 	   Funkverbindung: -56dBm      Akku: 36%    vor 13s
 * 	   letzte Auslösung Lichttaster: um 12:21:17.7897 vor 11s
 * Ziel:
 *     Funkverbindung: -56dBm      Akku: 36%    vor 2s
 *     letzte Auslösung Lichttaster L: um 12:21:17.7897 vor 17s
 *     letzte Auslösung Lichttaster R: um 12:21:17.7897 vor 123s
 * Zille 1:
 *     Funkverbindung: -56/-73dBm      Akku: 36%    vor 17s
 *     01) Start: 12:34:56.7890    Ziel: 13:34:56:7890    Dauer: 00:04:56.6789
 *     02) Start: 12:34:56.7890    Ziel: 13:34:56:7890    Dauer: 00:04:56.6789
 *     03) Start: 12:34:56.7890    läuft
 * Zille 2:
 *     Funkverbindung: -56/-32dBm      Akku: 36%    vor 16s
 *     11) Start: 12:34:56.7890    Ziel: 13:34:56:7890    Dauer: 00:04:56.6789
 *     12) Start: 12:34:56.7890    Ziel: 13:34:56:7890    Dauer: 00:04:56.6789
 *     13) Start: 12:34:56.7890    Ziel: 13:34:56:7890    Dauer: 00:04:56.6789
 */

#ifdef INFO
static void printInfoScreen()
{
	info.timeBase.time = time_100us;
	UpdateVoltages();
	info.timeBase.voltage = vMin*1000; // convert from Volts to Milli-Volts

	stream(RS232, "\n\nZeitbasis:\n");
	stream(RS232, "    Basiszeit: %s    Akku: %d%%\n",
			sprintHMSX(info.timeBase.time),
			VoltageToPercent(info.timeBase.voltage/1000.0f));

	int TS=0; // start:
	stream(RS232, "Start:\n");
	stream(RS232, "    Funkverbindung: %ddBm    Akku: %d%%   vor %ds\n",
			info.triggerStations[TS].rssi,
			VoltageToPercent(info.triggerStations[TS].voltage/1000.0f),
			(int)((time_100us-info.triggerStations[TS].tOfLastAlive)/10000));
	stream(RS232, "    letzte Ausloesung Lichttaster: um %s vor %ds\n",
			sprintHMSX(info.triggerStations[TS].triggerTimeL),
			(int)((info.timeBase.time-info.triggerStations[TS].triggerTimeL)/10000));

	TS = 1; // finish
	stream(RS232, "Ziel:\n");
	stream(RS232, "    Funkverbindung: %ddBm    Akku: %d%%   vor %ds\n",
			info.triggerStations[TS].rssi,
			VoltageToPercent(info.triggerStations[TS].voltage/1000.0f),
			(int)((time_100us-info.triggerStations[TS].tOfLastAlive)/10000));
	stream(RS232, "    letzte Ausloesung Lichttaster L: um %s vor %ds\n",
			sprintHMSX(info.triggerStations[TS].triggerTimeL),
			(int)((info.timeBase.time-info.triggerStations[TS].triggerTimeL)/10000));
	stream(RS232, "    letzte Ausloesung Lichttaster R: um %s vor %ds\n",
			sprintHMSX(info.triggerStations[TS].triggerTimeR),
			(int)((info.timeBase.time-info.triggerStations[TS].triggerTimeR)/10000));


	for(int z=0; z<2; z++)
	{
		stream(RS232, "Zille %d:\n", z+1);
		stream(RS232, "    Funkverbindung: %d/%ddBm    Akku: %d%%    vor %ds\n",
				info.boatBoxes[z].rssi[0],
				info.boatBoxes[z].rssi[1],
				VoltageToPercent(info.boatBoxes[z].voltage/1000.0f),
				(int)((time_100us-info.boatBoxes[z].tOfLastAlive)/10000));
		for(int i=0; i<N_LAPS; i++) // for all laps in history
		{
			if(i == N_LAPS-1 && info.boatBoxes[z].isRunning) // currently running...
			{
				stream(RS232, "    %d) Start: %s    laueft seit: %s\n",
						info.boatBoxes[z].laps[i],
						sprintHMSX(info.boatBoxes[z].startTimes[i]),
						sprintHMSX(time_100us - info.boatBoxes[z].startTimes[i]));
			}
			else // laps in history:
			{
				if(info.boatBoxes[z].startTimes[i] == info.boatBoxes[z].finishTimes[i]) // run that wasn't finished
				{
					stream(RS232, "    %d) Start: %s     Abbruch\n",
							info.boatBoxes[z].laps[i],
							sprintHMSX(info.boatBoxes[z].startTimes[i]));
				}
				else // completed lap in history:
				{
					stream(RS232, "    %d) Start: %s    Ziel: %s    Dauer: %s\n",
							info.boatBoxes[z].laps[i],
							sprintHMSX(info.boatBoxes[z].startTimes[i]),
							sprintHMSX(info.boatBoxes[z].finishTimes[i]),
							sprintHMSX(info.boatBoxes[z].finishTimes[i]-info.boatBoxes[z].startTimes[i]));
				}
			}
		}
	}
}
#endif


// used for TB <-> PC Communication
static void ConfigureRS232()
{
	// setup hardware:
	PIO_Configure(pPinsRS232, 2);
	USART_Configure(pUsartRS232,
		0<<0| // mode (0=normal)
		0<<4| // clock select (0=MCK)
		3<<6| // Character length (3=8bits)
		0<<8| // synch. mode (0=asynchronous)
		4<<9| // Parity type (4=no parity)
		0<<12| // num. of stop bits (0=1bit)
		0<<14| // channel mode (0=normal)
		0<<16| // bit order (0=LSB; 1=MSB first)
		0<<17| // 9-bit char len (0=char.length defines length)
		0<<18| // drive SCK output (0=no drive SCK)
		0<<19| // oversampling mode (0=16x; 1=8x)
		0<<20| // INACK inhibit non acknowledge (0=the NACK is generated)
		0<<21| // DSNACK disable successive NACK (0=not disable SNACK)
		2<<24| // MAX_ITERATION in ISO7816 T0 mode
		0<<28  // FILTER for IrDA (0=no filter)
		, 115200, BOARD_MCK);
	PMC_EnablePeripheral(BOARD_RS232_USART_ID); // enable clock USART RS232

//	// set up interrupt:
	const uint32_t mode = 5; // set priority to 5
	// mode = 0; i.e. priority = 0 << 0 (lowest)
	// 				  source type = 0 << 5 (level sensitive), is recommended for USART
	AIC_ConfigureIT(BOARD_RS232_USART_ID, mode, ISR_RS232);
	AIC_EnableIT(BOARD_RS232_USART_ID);
	pUsartRS232->US_IER = AT91C_US_RXRDY;//  AT91C_US_TXBUFE is polled in the main loop

	// enable receive and transmit
	USART_SetTransmitterEnabled(pUsartRS232, 1);
	USART_SetReceiverEnabled(pUsartRS232, 1);
}


// used for ALGE-Displays
static void ConfigureRS232_ALGE()
{
	// setup hardware:
	PIO_Configure(pPinsRS232_ALGE, 2);
	USART_Configure(pUsartRS232_ALGE,
		0<<0| // mode (0=normal)
		0<<4| // clock select (0=MCK)
		3<<6| // Character length (3=8bits)
		0<<8| // synch. mode (0=asynchronous)
		4<<9| // Parity type (4=no parity)
		0<<12| // num. of stop bits (0=1bit)
		0<<14| // channel mode (0=normal)
		0<<16| // bit order (0=LSB; 1=MSB first)
		0<<17| // 9-bit char len (0=char.length defines length)
		0<<18| // drive SCK output (0=no drive SCK)
		0<<19| // oversampling mode (0=16x; 1=8x)
		0<<20| // INACK inhibit non acknowledge (0=the NACK is generated)
		0<<21| // DSNACK disable successive NACK (0=not disable SNACK)
		2<<24| // MAX_ITERATION in ISO7816 T0 mode
		0<<28  // FILTER for IrDA (0=no filter)
		, 2400, BOARD_MCK);
	PMC_EnablePeripheral(BOARD_RS232_ALGE_USART_ID); // enable clock USART RS232

	// enable transmit
	USART_SetTransmitterEnabled(pUsartRS232_ALGE, 1);
}
