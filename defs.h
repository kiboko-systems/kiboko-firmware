/*
 *  defs.h
 *
 *  Created on: 06.07.2012
 *      Author: Karl Zeilhofer
 */

#ifndef DEFS_H_
#define DEFS_H_

#include <board.h>

#include <rf/rf.h> // time slot protocol handler
#include <trx/trx.h> // transceiver modules from TI
#include "dcl.h" // debug command line

#include <pio/pio.h>
#include <pio/pio_it.h>

#include <dbgu/dbgu.h>

#include <utility/led.h>
#include <utility/trace.h>
#include <rstc/rstc.h>
#include <spi/spi.h>
#include <adc/adc.h>

#include <usart/usart.h>
#include <pmc/pmc.h>
#include <aic/aic.h>
#include <pit/pit.h>
#include <pwmc/pwmc.h>
#include <tc/tc.h>

#include "packet.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define IGNORE_TRIGGER_DURATION_BB_100us 100000 // ignore triggering for 10s after a trigger occurred
#define IGNORE_TRIGGER_DURATION_TS_100us 30000 // ignore triggering for 3s after a trigger occurred
//#define RESET_SYNC_TIMEOUT_100us 18000000 // reset board, when the sync-flag is false for more than 30min
//#define RESET_ALIVE_TIMEOUT_100us 18000000 // reset board, when there was no alive-signal for more than 30min


#define NUM_OF_BOAT_BOXES 			25
#define NUM_OF_TRIGGER_STATIONS 	2
#define NUM_OF_MATRICES				2

#define TIC_INTERVAL_us (100) // number of microseconds for each time-tic
#define TICS_PER_SLOT_433M 	20 // 2ms
#define NUM_OF_SLOTS_433M 	(2+NUM_OF_TRIGGER_STATIONS+NUM_OF_MATRICES) // 6 channels (1 Time-Base, 1 Dummy Master, 2 Trigger-Stations, 2 Kiboko-Matrices)
#define TICS_PER_SLOT_2G4 	20 // 2ms
#define NUM_OF_SLOTS_2G4 	(NUM_OF_BOAT_BOXES+NUM_OF_TRIGGER_STATIONS) // 27 channels (25 Boat-Boxes, 2 Trigger-Stations)

// 433MHz: (ca. 600us)
#define TRANSMIT_DELAY_100us_433M 			(6) // 600us
#define TRANSMIT_DELAY_thirdMicroSec_433M 	(83) // 83/3 = 27.67us
// 2.4GHz: (ca. 600us)
#define TRANSMIT_DELAY_100us_2G4 			(6) // (600us)
#define TRANSMIT_DELAY_thirdMicroSec_2G4 	(68) // 68/3 = 22.33us


#define PIT_INTERVAL (300) // PIT runs with MCK/16, which gives @48MHz and 10kHz a Value of 300
#define PIT_MODE_REG ((PIT_INTERVAL-1)|AT91C_PITC_PITEN|AT91C_PITC_PITIEN) // MCK-TAG

//	// for this special way of defining a macro, refer to K.N.King
	#define PULSE_FAST_SYNC		do{*AT91C_PIOA_SODR = (1<<4);*AT91C_PIOA_CODR = (1<<4);}while(0)
	#define PULSE_SLOW_SYNC		do{*AT91C_PIOA_SODR = (1<<27);*AT91C_PIOA_CODR = (1<<27);}while(0)
	#define POWER_LED1_ON 		*AT91C_PIOA_SODR = (1<<0) // PA0
	#define POWER_LED2_ON 		*AT91C_PIOA_SODR = (1<<1) // PA1
	#define POWER_LED1_OFF 		*AT91C_PIOA_CODR = (1<<0) // PA0
	#define POWER_LED2_OFF 		*AT91C_PIOA_CODR = (1<<1	) // PA1
	#define RELAIS_ON			POWER_LED2_ON // Relais is used in Matrix-Board only, and the same pin as the external Power-LED
		// TODO: use other pin!
	#define RELAIS_OFF			POWER_LED2_OFF
	#define POWER_LED_PERIOD_START 900 // in us, use only multiple of 100us
	#define POWER_LED_PERIOD_GOAL1 1000 // in us, use only multiple of 100us,  internal Power-LED
	#define POWER_LED_PERIOD_GOAL2 1100 // in us, use only multiple of 100us, external Power-LED

#define KICK_DOG 	(AT91C_BASE_WDTC->WDTC_WDCR = 0xA5000001)


#endif /* DEFS_H_ */
