/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support 
 * ----------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

//------------------------------------------------------------------------------
/// \unit
/// !Purpose
/// 
/// Definition of AT91SAM7S-EK characteristics, AT91SAM7S-dependant PIOs and
/// external components interfacing.
/// 
/// !Contents
/// This file provide a large number of definitions, which are of three
/// different types.
///
/// PIO definitions are prefixed with #PIN_# or #PINS_#. They are to be used
/// with the pio peripheral to configure the pins required by the application.
///
/// First, additional information about the platform is provided by several
/// constants:
///    - BOARD_NAME is a string containing the board name
///    - The chip family and board (at91sam7s and at91sam7sek) are also
///      provided.
///    - BOARD_MAINOSC and BOARD_MCK contains the standard frequency for the
///      main oscillator and the master clock.
///
/// Contants prefixed with #BOARD_USB_# give information about the USB device
/// peripheral that is provided in the chip.
///
/// Defines prefixed with #PIN_# contain only one pin (and thus can be safely
/// used to initialize a single Pin instance), whereas defines starting with
/// #PINS_# contains either a single Pin instance with multiple pins inside it,
/// or a list of several Pin instances; they must be used as Pin[] array
/// initializer values, otherwise they are not safe.
///
/// Finally, some information about the flash controller is given by definitions
/// prefixed with #BOARD_FLASH_#.
//------------------------------------------------------------------------------

/********** BOARD DESCRIPTION **************************************************
 *	This board is part of the project RF-StopWatch
 *		it is supplyed by two lithium-ion battery cells (type 18650, 3.7V nominal and 2500mAh)
 *		it has a Atmel AT91SAM7S2
 */

/*
 * Pin Usage:
 * noch offen: IRQ vom RxTx-Modul, 6 mal
 * Batteriespannung -> AD4
 * 5 pins ID-Jumper??
 *
 * PA0:		PowerLED1 MOSFET-Gate (high drive)
 * PA1:		PowerLED2 MOSFET-Gate (high drive)
 * PA2:		LED4
 * PA3:		LED3
 * PA4:		Fast-Sync-Pin (output)
 * PA5:		RXD0 (reserve)
 * PA6:		TXD0 (reserve)
 * PA7:		GD2_A
 * PA8:		GD0_A
 * PA9:		DRXD (Debug Unit)
 * PA10:	DTXD (Debug Uinit)
 * PA11:	NPCS0 (SPI, Transceiver 1)
 * PA12:	MISO
 * PA13:	MOSI
 * PA14:	SPCK
 * PA15:
 * PA16:
 * PA17:
 * PA18:	LED1
 * PA19:	Button1
 * PA20:
 * PA21:
 * PA22:	NPCS3 (reserve)
 * PA23:
 * PA24:	TRIG (Photoreceiver)
 * PA25:	GD2_B
 * PA26:	GD0_B
 * PA27:	Slow-Sync-Pin (output)
 * PA28:	Light-Sensor Left
 * PA29:	Light-Sensor Right
 * PA30:	LED2
 * PA31:	NPCS1 (SPI, Trasceiver 2)
 */

#ifndef BOARD_H 
#define BOARD_H

//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#if defined(at91sam7s32)
    #include "at91sam7s32/AT91SAM7S32.h"
#elif defined(at91sam7s321)
    #include "at91sam7s321/AT91SAM7S321.h"
#elif defined(at91sam7s64)
    #include "at91sam7s64/AT91SAM7S64.h"
#elif defined(at91sam7s128)
    #include "at91sam7s128/AT91SAM7S128.h"
#elif defined(at91sam7s256)
    #include "at91sam7s256/AT91SAM7S256.h"
#elif defined(at91sam7s512)
    #include "at91sam7s512/AT91SAM7S512.h"
#else
    #error Board does not support the specified chip.
#endif

//------------------------------------------------------------------------------
//         Global Definitions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Board
//------------------------------------------------------------------------------
/// String containing the name of the board.
#define BOARD_NAME      "AT91SAM7S-RFSW" // Radio Frequency Stop Watch
/// Board definition.
#define at91sam7sek
/// Family definition.
#define at91sam7s
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Clocks
//------------------------------------------------------------------------------
/// Frequency of the board main oscillator, in Hz.
#define BOARD_MAINOSC           20000000//18432000

/// Master clock frequency (when using board_lowlevel.c), in Hz.
#define BOARD_MCK               48000000
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// ADC
//------------------------------------------------------------------------------
/// ADC clock frequency, at 10-bit resolution (in Hz)
#define ADC_MAX_CK_10BIT         5000000
/// ADC clock frequency, at 8-bit resolution (in Hz)
#define ADC_MAX_CK_8BIT          8000000
/// Startup time max, return from Idle mode (in �s)
#define ADC_STARTUP_TIME_MAX       20
/// Track and hold Acquisition Time min (in ns)
#define ADC_TRACK_HOLD_TIME_MIN   600

//------------------------------------------------------------------------------
// USB
//------------------------------------------------------------------------------
/// Indicates the chip has a UDP controller.
#define BOARD_USB_UDP

/// Indicates the D+ pull-up is externally controlled.
#define BOARD_USB_PULLUP_EXTERNAL

/// Number of endpoints in the USB controller.
#define BOARD_USB_NUMENDPOINTS                  4

/// Returns the maximum packet size of the given endpoint.
/// \param i  Endpoint number.
/// \return Maximum packet size in bytes of endpoint. 
#define BOARD_USB_ENDPOINTS_MAXPACKETSIZE(i)    ((i == 0) ? 8 : 64)

/// Returns the number of FIFO banks for the given endpoint.
/// \param i  Endpoint number.
/// \return Number of FIFO banks for the endpoint.
#define BOARD_USB_ENDPOINTS_BANKS(i)            (((i == 0) || (i == 3)) ? 1 : 2)

/// USB attributes configuration descriptor (bus or self powered, remote wakeup)
#define BOARD_USB_BMATTRIBUTES                  USBConfigurationDescriptor_SELFPOWERED_NORWAKEUP
//#define BOARD_USB_BMATTRIBUTES                  USBConfigurationDescriptor_BUSPOWERED_NORWAKEUP
//#define BOARD_USB_BMATTRIBUTES                  USBConfigurationDescriptor_SELFPOWERED_RWAKEUP
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Pins
//------------------------------------------------------------------------------
/// DBGU pins definition. Contains DRXD (PA9) and DTXD (PA10).
#define PINS_DBGU  {0x00000600, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}

/// LED #1 pin definition (PA18).
#define PIN_LED_DS1   {1 << 18, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
/// LED #2 pin definition (PA30).
#define PIN_LED_DS2   {1 << 30, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
/// LED #3 pin definition (PA3).
#define PIN_LED_DS3   {1 <<  3, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
/// LED #4 pin definition (PA2).
#define PIN_LED_DS4   {1 <<  2, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
/// LED #5 pin definition (PA1).
//#define PIN_LED_DS5   {1 <<  1, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
/// List of the five LED pin definitions
#define PINS_LEDS   PIN_LED_DS1, PIN_LED_DS2, PIN_LED_DS3, PIN_LED_DS4 //, PIN_LED_DS5
// led index
#define LED_DS1		(0) // indicates synchronized-flag
#define LED_DS2		(1) // TRX-A: packet received
#define LED_DS3		(2) // TRX-A: packet transmitted
#define LED_DS4		(3) // TRX-B: packet received
//#define LED_DS5		(4) // TRX-B: packet transmitted

#define LED_DS_RxA		(1) // TRX-A: packet received
#define LED_DS_TxA		(2) // TRX-A: packet transmitted
#define LED_DS_RxB		(3) // TRX-B: packet received
//#define LED_DS_TxB		(4) // TRX-B: packet transmitted


/// Power-LED pin definition (PA0).
#define PIN_POWER_LED1  {1 << 0, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_POWER_LED2  {1 << 1, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}

#define PIN_RELAIS 		PIN_POWER_LED2

/// Fast-Sync-Pin definition (PA4). This pin is pulsed high as short as possible in the PIT-IRQ
#define PIN_FAST_SYNC  {1 << 4, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}
/// Slow-Sync-Pin definition (PA27). This pin is pulsed high as short as possible periodically (about once a second)
#define PIN_SLOW_SYNC  {1 << 27, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}

/// Push button #0 definition (PA19).
#define PIN_PUSHBUTTON_1    {1 << 19, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH | PIO_PULLUP}
/// List of all push button definitions (PA19).
#define PINS_PUSHBUTTONS    PIN_PUSHBUTTON_1
/// Push button #1 index.
#define PUSHBUTTON_BP1      0

/// LIGHT-SENSOR on TRIGGER-STATION:
// are driven with 1kOhm in Series, as protection!
// PA28	Input	Light-Sensor Left
#define PIN_LIGHT_SENSOR_L 		{1 << 28, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH} // CAUTION!! DO NOT ENABLE THE PULLUP, see ERRATA from ATMEL
// PA29	Input	Light-Sensor Right
#define PIN_LIGHT_SENSOR_R 		{1 << 29, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH}  // CAUTION!! DO NOT ENABLE THE PULLUP, see ERRATA from ATMEL
#define PINS_LIGHT_SENSORS		PIN_LIGHT_SENSOR_L, PIN_LIGHT_SENSOR_R

// PA28	Input	Light-Sensor Left
#define PIN_OFF_SWITCH 		{1 << 28, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH | PIO_PULLUP} // use PA28 for light-sensor left in trigger-station and for off-switch in matrix-board


// !internal ADC
#define BOARD_VBAT_BASE		AT91C_BASE_ADC
#define BOARD_VBAT_ID		AT91C_ID_ADC
#define BOARD_VBAT_CH1		4 // Li-Cell 1
#define BOARD_VBAT_CH12		5 // Li-Cells 1+2
#define BOARD_VBAT_CH123	6 // Li-Cells 1+2+3
#define ADC_STARTUP_TIME_MAX 		20
#define ADC_TRACK_HOLD_TIME_MIN		600

/// USART0 TXD pin definition (PA5).
#define PIN_USART0_RXD  {1 << 5, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// USART0 RXD pin definition (PA6).
#define PIN_USART0_TXD  {1 << 6, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}

/// USART1 RXD pin definition (PA21).
#define PIN_USART1_RXD  {1 << 21, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// USART1 TXD pin definition (PA22).
#define PIN_USART1_TXD  {1 << 22, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}


// !RS232 (using USART0)
#define BOARD_RS232_USART_BASE	AT91C_BASE_US0
#define BOARD_RS232_USART_ID	AT91C_ID_US0
#define BOARD_RS232_TXD_PIN		PIN_USART0_TXD
#define BOARD_RS232_RXD_PIN		PIN_USART0_RXD
#define BOARD_RS232_PINS		BOARD_RS232_TXD_PIN, BOARD_RS232_RXD_PIN


// !RS232 (using USART1)
#define BOARD_RS232_ALGE_USART_BASE		AT91C_BASE_US1
#define BOARD_RS232_ALGE_USART_ID		AT91C_ID_US1
#define BOARD_RS232_ALGE_TXD_PIN		PIN_USART1_TXD
#define BOARD_RS232_ALGE_RXD_PIN		PIN_USART1_RXD
#define BOARD_RS232_ALGE_PINS			BOARD_RS232_ALGE_TXD_PIN, BOARD_RS232_ALGE_RXD_PIN


/// SPI MISO pin definition (PA12).
#define PIN_SPI_MISO   {1 << 12, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_PULLUP}
/// SPI MOSI pin definition (PA13).
#define PIN_SPI_MOSI   {1 << 13, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// SPI SPCK pin definition (PA14).
#define PIN_SPI_SPCK   {1 << 14, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// SPI pins definition. Contains MISO, MOSI & SPCK (PA12, PA13 & PA14).
#define PINS_SPI       PIN_SPI_MISO, PIN_SPI_MOSI, PIN_SPI_SPCK
/// SPI chip select 0 pin definition (PA11).
#define PIN_SPI_NPCS0  {1 << 11, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// SPI chip select 1
#define PIN_SPI_NPCS1  {1 << 31, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// SPI chip select 2
#define PIN_SPI_NPCS2  {1 << 10, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}
/// SPI chip select 3
#define PIN_SPI_NPCS3  {1 <<  3, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}

/// PWMC PWM0 pin definition (PA1).
#define PIN_PWMC_PWM1  {1 << 1, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
/// PWMC PWM0 pin definition (PA2).
#define PIN_PWMC_PWM2  {1 << 2, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}

/// PWM pin definition for LED0
#define PIN_PWM_LED0 PIN_PWMC_PWM1
/// PWM pin definition for LED1
#define PIN_PWM_LED1 PIN_PWMC_PWM2
/// PWM channel for LED0
#define CHANNEL_PWM_LED0 1
/// PWM channel for LED1
#define CHANNEL_PWM_LED1 2

/// TWI pins definition. Contains TWD (PA3) and TWCK (PA4).
#define PINS_TWI  {0x00000018, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}

/// ADC_AD0 pin definition.
#define PIN_ADC_AD0 {1 << 17, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}
/// ADC_AD1 pin definition.
#define PIN_ADC_AD1 {1 << 18, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}
/// ADC_AD2 pin definition. (mixed with PIN_PUSHBUTTON_1)
#define PIN_ADC_AD2 {1 << 19, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}
/// ADC_AD3 pin definition. (mixed with PIN_PUSHBUTTON_2)
#define PIN_ADC_AD3 {1 << 20, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}
/// Pins ADC
#define PINS_ADC PIN_ADC_AD0, PIN_ADC_AD1, PIN_ADC_AD2, PIN_ADC_AD3

/// USB VBus monitoring pin definition (PA13).
#define PIN_USB_VBUS    {1 << 13, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}
/// USB pull-up control pin definition (PA16).
#define PIN_USB_PULLUP  {1 << 16, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
//------------------------------------------------------------------------------

/// !SD Card SPI
/// - BOARD_SD_SPI_BASE
/// - BOARD_SD_SPI_ID  
/// - BOARD_SD_SPI_PINS
/// - BOARD_SD_NPCS    

/// Not define in our board, but customer can add this feature
/// Base address of the SPI peripheral connected to the SD card.
#define BOARD_SPI_BASE   AT91C_BASE_SPI
/// Identifier of the SPI peripheral connected to the SD card.
#define BOARD_SPI_ID     AT91C_ID_SPI
/// List of pins to configure to access the SD card
#define BOARD_SPI_PINS   PINS_SPI, PIN_SPI_NPCS0, PIN_SPI_NPCS1, PIN_SPI_NPCS2, PIN_SPI_NPCS3
/// NPCS number:
#define BOARD_TRX_A_SPI_CLK	(6000000); // SPI Clock Speed in Hz // TODO 5: set back to 9MHz, if possible!!
#define BOARD_TRX_B_SPI_CLK	(6000000); // SPI Clock Speed in Hz

/// NPCS Numbers:
#define BOARD_TRX_A_NPCS	0 // for 2.4 GHz Transceiver module A (CC2500)
#define BOARD_TRX_B_NPCS	1 // for 433 MHz Transceiver module B (CC1101)
/// NPCS pins:
#define BOARD_TRX_A_NPCS_PIN	PIN_SPI_NPCS0 // for 2.4 GHz Transceiver module A (CC2500)
#define BOARD_TRX_B_NPCS_PIN	PIN_SPI_NPCS1 // for 433 MHz Transceiver module B (CC1101)
/// General Data I/O Pins:
#define BOARD_TRX_A_GD0_PIN {1 << 8, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH | PIO_PULLUP}
#define BOARD_TRX_A_GD2_PIN {1 << 7, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH | PIO_PULLUP}
#define BOARD_TRX_B_GD0_PIN {1 << 26, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH | PIO_PULLUP}
#define BOARD_TRX_B_GD2_PIN {1 << 25, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH | PIO_PULLUP}

// Trigger Input from Photoreceiver
#define PIN_TRIG {1 << 24, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEGLITCH}

// Pins for RF-Module A (CC2500):
#define BOARD_TRX_A_PINS 		PIN_SPI_MISO, PIN_SPI_MOSI, PIN_SPI_SPCK, BOARD_TRX_A_NPCS_PIN, BOARD_TRX_A_GD0_PIN, BOARD_TRX_A_GD2_PIN
#define BOARD_TRX_B_PINS 		PIN_SPI_MISO, PIN_SPI_MOSI, PIN_SPI_SPCK, BOARD_TRX_B_NPCS_PIN, BOARD_TRX_B_GD0_PIN, BOARD_TRX_B_GD2_PIN
//------------------------------------------------------------------------------

// Flash
//------------------------------------------------------------------------------
/// Indicates chip has an EFC.
#define BOARD_FLASH_EFC
/// Address of the IAP function in ROM.
#define BOARD_FLASH_IAP_ADDRESS         0x300E08
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// \page "SAM7S-EK - External components"
/// This page lists the definitions related to external on-board components
/// located in the board.h file for the SAM7S-EK.
/// 
/// !ISO7816
/// - PIN_SMARTCARD_CONNECT
/// - PIN_ISO7816_RSTMC
/// - PINS_ISO7816

/// Smartcard detection pin
#define PIN_SMARTCARD_CONNECT   {1 << 5, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_INPUT, PIO_DEFAULT}
/// PIN used for reset the smartcard
#define PIN_ISO7816_RSTMC       {1 << 7, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}
/// Pins used for connect the smartcard
#define PINS_ISO7816            PIN_USART0_TXD, PIN_USART0_SCK, PIN_ISO7816_RSTMC
//------------------------------------------------------------------------------

#endif //#ifndef BOARD_H

