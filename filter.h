/*
 * Karl Zeilhofer
 * 23.8.2010
 *
 *
 * Version History:
 * 	23.8.2010:	V0.1
 * 				First implementations
 *
 */

#ifndef FILTER_H
#define FILTER_H

#include <malloc.h>
#include <stdint.h>

typedef float FILT_T;

// Butterworth 5th order, fs=2kHz, fc=10Hz
#define B_BW5_2K_10 {908.916537683512e-012,4.54458268841756e-009,9.08916537683512e-009,9.08916537683512e-009,4.54458268841756e-009,908.916537683512e-012}
#define A_BW5_2K_10 {1.00000000000000e+000,-4.89834550570808e+000,9.59852968565134e+000,-9.40535565316515e+000,4.60850734092150e+000,-903.335838614280e-003}

// Butterworth 5th order, fs=2kHz, fc=50Hz
#define B_BW5_2K_50 {2.31822440836601e-006,11.5911220418301e-006,23.1822440836601e-006,23.1822440836601e-006,11.5911220418301e-006,2.31822440836601e-006}
#define A_BW5_2K_50 {1.00000000000000e+000,-4.49287138204599e+000,8.09770351863423e+000,-7.31689784255498e+000,3.31388439633507e+000,-601.744507187259e-003}



typedef struct
{
	uint8_t orderNum; // order of the numerator polynomial
	uint8_t orderDen; // order of the denominator polynomial

	FILT_T* b; // coefficients of the numerator
	FILT_T* a; // coefficients of the denominator
		// a[0] = is not used (equals 1)

	FILT_T* x; // buffer for recent input samples
	FILT_T* y; // buffer for recent output samples
		// most recent samples on x[0].
		// oldest samples on x[orderNum-1];
}FILT_D; // data structure for a filter-instance


FILT_D* FILT_New(int orderNum, int orderDenom);
void FILT_SetCoeffs(FILT_D* f, FILT_T* b, FILT_T* a);
FILT_T FILT_Filter(FILT_D* pFlt, FILT_T x0);


#endif
