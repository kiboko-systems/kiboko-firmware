/*
 * boatboxbuffer.c
 *
 *  Created on: Aug 13, 2013
 *      Author: Karl Zeilhofer
 */


#include "boatboxbuffer.h"

#include "defs.h"

#include <stdint.h>
#include <stdbool.h>
#include "packet_functions.h"


typedef struct _object
{
	ALIVE_PACKET packets[NUM_OF_BOAT_BOXES];

	int priorities[NUM_OF_BOAT_BOXES];
		// if priority = -1, the packet isn't valid
		// if priority = 0, the packet is old, and has the same priority as most of the others.
		// if priority > 0, it is preferred returned by the function BBB_getPacket()
		// on each return, the number is reduced by one until zero.

	int nBoxes; // abbrevation for NUM_OF_BOAT_BOXES
	int recentIndex; // index of the packet, that was recently returned by BBB_getPacket()
}Object;

static Object self; // private object
static const ALIVE_PACKET defaultPacket =
{
		.batteryVoltageTS = 0xffff,
		.triggerTimeTS_L = TIME_INVALID,
		.triggerTimeTS_R = TIME_INVALID,
		.boatBoxID = 0xFF,
		.batteryVoltageBB = 0xffff,
		.triggerTimeBB = TIME_INVALID, // send the last trigger time
		.binRssiBB = 0,
		.stationTriggeredAt=0
};

void BBB_init()
{
	self.nBoxes = NUM_OF_BOAT_BOXES;
	self.recentIndex = 0;
	for(int n=0; n<self.nBoxes; n++)
	{
		self.priorities[n]=-1; // set all packets to invalid.
	}
}

// insert a packet into the buffer.
// it automatically checks, if the packet has new content.
// if so, this packet gets higher priority, and will then be
// preferred by the function BBB_getPacket()
// the priority is only for new time stamps increased.
// battery status doesn't lead to a higher priority.
void BBB_insertPacket(ALIVE_PACKET p)
{
	// TODO 3: ensure buffer data integrety!!!, called from an ISR!
	// perhaps this is the source of spurious time stamps!

	int i = p.boatBoxID-1; // ID starts at 1, index i starts at 0

	// if new and valid time stamp
	if(p.triggerTimeBB != TIME_INVALID &&
		p.triggerTimeBB != self.packets[i].triggerTimeBB)
	{
		self.priorities[i]=BBB_NEW_TIMESTAMP_PRIORITY;
		//printf("got packet with new time stamp\n");
	}
	else
	{
		self.priorities[i]=BBB_NEW_PACKET_PRIORITY; // send other packet
	}
	self.packets[i] = p; // store the packet
}

// this function returns a packet from the buffer
// new packets are preferred.
// if a new packet was returned a few times, then old packets
// will be returned in sequence.
// if no valid packet is in the buffer (on startup), a default packet is returned
// when this function is called, we assume, that the packet returned also gets sent!
// priority returns the priority of the packet returned,
// so the caller can adopt the packetrate according to the priority.
// it is used to reduce the packet rate in idle states of the RF-Network.
ALIVE_PACKET BBB_getPacket(int* priority)
{
	// TODO 3: ensure buffer data integrety!!!, ISR changes buffer content!
	// perhaps this is the source of spurious time stamps!

	int i = self.recentIndex;

	static int recentReturnedIndex = -1;

	int highestPriorityIndex = recentReturnedIndex+1; // this is needed to walk through the same priorities
	if(highestPriorityIndex>=self.nBoxes)
	{
		highestPriorityIndex=0;
	}
	int c=0; // search counter, to prevent endless loop, and enable starting at any index
	for(int i=highestPriorityIndex; c<self.nBoxes; i++,c++)
	{
		if(i>=self.nBoxes)
		{
			i=0;
		}
		if(self.priorities[i] > self.priorities[highestPriorityIndex])
		{
			highestPriorityIndex = i;
		}
	}
	i = highestPriorityIndex;

	recentReturnedIndex = i;

	if(self.priorities[i] > 0)
	{
		*priority = self.priorities[i];
		self.priorities[i]--; // decrement priority
		return self.packets[i];
	}
	else
	{
		*priority = 0;
		return defaultPacket;
	}
}

void BBB_printBuffer()
{
	printf("Boat Box Buffer\n");
	printf("    recentIndex = %d\n", self.recentIndex);
	printf("    nBoxes = %d\n", self.nBoxes);
	printf("    \n");
	for(int n=0; n<NUM_OF_BOAT_BOXES; n++)
	{
		printf("packets[%d] priority = %d:\n", n, self.priorities[n]);
		if(self.priorities[n]>=0)
		{
			PACKET_printAlivePacket(&self.packets[n]);
		}
	}
}
